<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<%
	String user = null;
	String userId = null;
	Short stype = null;
	if (session != null) {
		user = (String) session.getAttribute("user");
		userId = (String) session.getAttribute("userId");
		stype = (Short) session.getAttribute("stype");
	}
%>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- css -->
<link rel="stylesheet" href="./assets/toast/toast.css">
<link rel="stylesheet" href="./assets/bootstrap/dist/css/bootstrap.css">
<link rel="stylesheet"
	href="./assets/bootstrap-table/bootstrap-table.css">
<link rel="stylesheet" href="./assets/Font-Awesome/css/font-awesome.css">
<link rel="stylesheet" href="./assets/common.css">
<!-- js -->
<script type="text/javascript"
	src="./assets/jQuery/jquery-1.10.2.min.js"></script>
<script type="text/javascript"
	src="./assets/bootstrap/dist/js/bootstrap.js"></script>
<script type="text/javascript"
	src="./assets/bootstrap-table/bootstrap-table.js"></script>
<script type="text/javascript"
	src="./assets/bootstrap-table/bootstrap-table-zh-CN.js"></script>
<script type="text/javascript" src="./assets/toast/toast.js"></script>
<script type="text/javascript" src="./assets/common.js"></script>
<title>班级管理</title>
</head>
<body>
	<div class="head">
		<h2 class="title">班级管理</h2>
	</div>
	<div class="container">
		<div class="col-lg-12" style="padding: 0 50px;">
			<div class="col-md-5" style="width: 30%;">
				<label for="modal" class="col-sm-2" style="width: 90px;">班级名称</label>
				<div class="col-sm-8" style="padding: 0px;">
					<input type="text" class="form-control" id="name" placeholder="班级名称"/>
				</div>
			</div>
			<div class="col-md-3">
				<label for="status" class="col-sm-3">年级</label>
				<div class="col-sm-6" style="padding: 0px">
					<select class="form-control" id="grade">
						<option value="">请选择</option>
						<option value="1">1年级</option>
						<option value="2">2年级</option>
						<option value="3">3年级</option>
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<label for="status" class="col-sm-3" style="width: 90px;">所属专业</label>
				<div class="col-sm-6" style="padding: 0px">
					<select class="form-control" id="major">
						<option value="">请选择</option>
						<option value="化学">化学</option>
						<option value="物理">物理</option>
					</select>
				</div>
			</div>
			<div class="col-md-1" style="width: 20%;">
				<button class="btn btn-primary" id="query" style="float: right;">查询</button>
			</div>
		</div>
		<div class="col-lg-12" style="padding: 0 50px;">
			<div class="col-md-12 toolbar"
				style="margin-top: 25px; margin-bottom: 5px;">
				<button class="btn btn-default" id="insert">
					<i class="fa fa-plus-square"></i> 新增
				</button>
				<button class="btn btn-default" id="edit"">
					<i class="fa fa-pencil-square"></i> 编辑
				</button>
				<button class="btn btn-default" id="delete">
					<i class="fa fa-times-circle"></i> 删除
				</button>
			</div>
			<div class="col-md-12">
				<table class="table-bordered" id="table"></table>
			</div>
		</div>
	</div>

	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">班级修改</h4>
				</div>
				<div class="modal-body">
					<form onsubmit="return false" action="##" method="post" id="form1">
						<input type="hidden" class="form-control" id="classId" name="id">
						<div class="form-group">
							<label for="nameedit">班级名称</label> <input type="text"
								class="form-control" id="nameedit" name="name"
								placeholder="班级名称" required>
						</div>
						<div class="form-group">
							<label for="gradeedit">年级</label> <select class="form-control"
								id="gradeedit" name="grade">
								<option value="">请选择</option>
								<option value="1">1年级</option>
								<option value="2">2年级</option>
								<option value="3">3年级</option>
							</select>
						</div>
						<div class="form-group">
							<label for="majoredit">所属专业</label> <select class="form-control"
								id="majoredit" name="majorid">
								<option value="">请选择</option>
								<option value="化学">化学</option>
								<option value="物理">物理</option>
							</select>
						</div>
						<div class="modal-footer">
							<input id="submitButton" type="button"
								onclick="submitForm('./editClass')" class="btn btn-success"
								value="提交">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">关闭</button>
						</div>
					</form>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
</body>

<script type="text/javascript">
$(document).ready(function () {
	    initTable();
	    $("#query").on("click", function () {
		var name  = $("#name").val();
		var grade  = $("#grade").val();
		var major  = $("#major").val();
	        $.ajax({
	            type: "post",
	            url: "./queryClass",
	            data: {name:name,grade:grade,major:major},
	            dataType: "json",
	            success: function (info) {
	                if (info.status > 0) {
	                    $("#table").bootstrapTable("load", info.data);
	                } else {
	                    showTip(info.msg, false);
	                }
	            },
	            error: function (error) {
	                showTip("服务器异常，请联系管理员！", false);
	            }
	        })
	    });
	    $("#insert").on("click", function () {
	        $("#myModalLabel").html("新增班级");
	        $("#myModal #classId").val("");
	        $("#myModal #nameedit").val("");
	        $("#myModal #gradeedit").val("");
	        $("#myModal #majoredit").val("");
	        $("#submitButton").attr("onclick", "submitForm('./addClass')");
	        $("#myModal").modal("show");
	    });
	    $("#edit").on("click", function () {
	        var selections = $("#table").bootstrapTable("getSelections");
	        if (selections.length == 0) {
	            showTip("无选中内容", false);
	        } else {
	            if (selections.length > 1) {
	                showTip("最多选择一项", false);
	            } else {
		        	$("#myModalLabel").html("编辑班级");
	                $("#myModal #classId").val(selections[0].id);
	                $("#myModal #nameedit").val(selections[0].name);
	                $("#myModal #gradeedit").val(selections[0].grade);
	                $("#myModal #majoredit").val(selections[0].majorid);
	                $("#submitButton").attr("onclick", "submitForm('./editClass')");
	                $("#myModal").modal("show");
	            }
	        }
	    });
	    $("#delete").on("click", function () {
	        var selections = $("#table").bootstrapTable("getSelections");
	        if (selections.length == 0) {
	            showTip("无选中内容", false);
	        } else {
	            if (selections.length > 1) {
	                showTip("最多选择一项", false);
	            } else {
	                var classId = selections[0].id;
	                $.ajax({
	                    type: "post",
	                    url: "./delClass",
	                    data: {
	                        id: classId
	                    },
	                    dataType: "json",
	                    success: function (info) {
	                        if (info.status > 0) {
	                            showTip(info.msg, true);
	                        } else {
	                            showTip(info.msg, false);
	                        }
							$("#query").click();
	                    },
	                    error: function (error) {
	                        showTip(error.msg, false);
	                    }
	                })
	            }
	        }
	    })
	});
	 
	function submitForm(url) {
	    $.ajax({
	        type: "post",
	        dataType: "json",
	        url: url,
	        data: $("#form1").serialize(),
	        success: function (info) {
	            $("#myModal").modal("hide");
	            if (info.status > 0) {
	                showTip(info.msg, true);
	            } else {
	                showTip(info.msg, false);
	            }
				$("#query").click();
	        },
	        error: function (error) {
	            showTip(error.msg, false);
	        }
	    })
	}
	function initTable() {
	    $("#table").bootstrapTable("destroy");
	    $("#table").bootstrapTable({
	        dataType: "json",
	        url: "./queryClass",
	        method: "get",
	        cache: false,
	        pagination: true,
	        columns: [{
	                field: "state",
	                checkbox: true,
	                align: "center",
	                valign: "middle"
	            }, {
	                field: "id",
	                title: "班级id",
	                visible: false
	            }, {
	                field: "name",
	                title: "模块名称",
	                align: "center",
	                valign: "middle"
	            }, {
	                field: "grade",
	                title: "年级",
	                align: "center",
	                valign: "middle"
	            }, {
	                field: "majorid",
	                title: "所属专业",
	                align: "center",
	                valign: "middle"
	            }],
	        onLoadSuccess: function (info) {
	            console.log(info);
	            if (info.status > 0) {
	                $("#table").bootstrapTable("load", info.data);
	            } else {
	                showTip(info.msg, false);
	            }
	        },
	        onLoadError: function (status) {
	            return false;
	        },
	    })
	};
</script>
</html>