<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<%
	String user = null;
	String userId = null;
	Short stype = null;
	if (session != null) {
		user = (String) session.getAttribute("user");
		userId = (String) session.getAttribute("userId");
		stype = (Short) session.getAttribute("stype");
	}
%>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- css -->
<link rel="stylesheet" href="./assets/toast/toast.css">
<link rel="stylesheet" href="./assets/bootstrap/dist/css/bootstrap.css">
<link rel="stylesheet"
	href="./assets/bootstrap-table/bootstrap-table.css">
<link rel="stylesheet" href="./assets/Font-Awesome/css/font-awesome.css">
<link rel="stylesheet" href="./assets/common.css">
<!-- js -->
<script type="text/javascript"
	src="./assets/jQuery/jquery-1.10.2.min.js"></script>
<script type="text/javascript"
	src="./assets/bootstrap/dist/js/bootstrap.js"></script>
<script type="text/javascript"
	src="./assets/bootstrap-table/bootstrap-table.js"></script>
<script type="text/javascript"
	src="./assets/bootstrap-table/bootstrap-table-zh-CN.js"></script>
<script type="text/javascript" src="./assets/toast/toast.js"></script>
<script type="text/javascript" src="./assets/common.js"></script>
<title>已选选课管理</title>
</head>
<body>
	<div class="head">
		<h2 class="title">已选选课管理</h2>
	</div>
	<div class="container">
		<div class="col-lg-12" style="padding: 0 50px;">
			<div class="col-md-5" style="width: 30%;">
				<label for="modal" class="col-sm-2" style="width: 90px;">学号</label>
				<div class="col-sm-8" style="padding: 0px;">
					<input type="text" class="form-control" id="studentid"
						placeholder="学号" />
				</div>
			</div>
			<div class="col-md-3">
				<label for="status" class="col-sm-3">课程</label>
				<div class="col-sm-6" style="padding: 0px">
					<select class="form-control" id="courseid">
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<label for="status" class="col-sm-3" style="width: 90px;">学期</label>
				<div class="col-sm-6" style="padding: 0px">
					<select class="form-control" id="semester">
						<option value="">请选择</option>
						<option value="1">第一学期</option>
						<option value="2">第二学期</option>
					</select>
				</div>
			</div>
			<div class="col-md-1" style="width: 20%;">
				<button class="btn btn-primary" id="query" style="float: right;">查询</button>
			</div>
		</div>
		<div class="col-lg-12" style="padding: 0 50px;">
			<div class="col-md-12 toolbar"
				style="margin-top: 25px; margin-bottom: 5px;">
				<% if(!"admin".equals(userId)){ %>
				<button class="btn btn-default" id="insert">
					<i class="fa fa-plus-square"></i> 选课
				</button>
				<%} %>
				<button class="btn btn-default" id="delete">
					<i class="fa fa-times-circle"></i> 退课
				</button>
				<% if(stype==1){ %>
				<button class="btn btn-default" id="edit"">
					<i class="fa fa-pencil-square"></i>选课修改
				</button>
				<%} %>
			</div>
			<div class="col-md-12">
				<table class="table-bordered" id="table"></table>
			</div>
		</div>
	</div>

	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">课程选择</h4>
				</div>
				<div class="modal-body">
					<form onsubmit="return false" action="##" method="post" id="form1">
						<input type="hidden" class="form-control" id="sxoreid" name="id">
						<input type="hidden" class="form-control" id="stuid"
							name="studentid">
						<% if(stype==1){ %>
						<div class="form-group">
							<label for="nameedit">姓名</label> <input type="text"
								class="form-control" id="studentnameedit" name="studentname"
								placeholder="姓名" disabled="disabled">
						</div>
						<%} %>
						<div class="form-group">
							<label for="gradeedit">课程</label> <select class="form-control"
								id="courseidedit" name="courseid">
							</select>
						</div>

						<div class="form-group">
							<label for="nameedit">学分</label> <input type="text"
								class="form-control" id="creditouredit" name="creditour"
								placeholder="学分" required>
						</div>
						<div class="form-group">
							<label for="nameedit">学期</label> <select class="form-control"
								id="semesteredit" name="semester">
								<option value="">请选择</option>
								<option value="1">第一学期</option>
								<option value="2">第二学期</option>
							</select>
						</div>
						<div class="form-group">
							<label for="gradeedit">讲授学时</label> <input type="text"
								class="form-control" id="classhouredit" name="classhour"
								placeholder="讲授学时" required>
						</div>
						<div class="form-group">
							<label for="gradeedit">实验学时</label> <input type="text"
								class="form-control" id="practicehouredit" name="practicehour"
								placeholder="实验学时" required>
						</div>
						<div class="form-group">
							<label for="majoredit">备注</label> <input type="text"
								class="form-control" id="remarkedit" name="remark"
								placeholder="备注">
						</div>
						<div class="modal-footer">
							<button id="submitButton" type="button"
								onclick="submitForm('./editSxore')" class="btn btn-success"
								>提交</button>
							<button type="button" class="btn btn-default"
								data-dismiss="modal">关闭</button>
						</div>
					</form>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
</body>

<script type="text/javascript">
var username = '<%=user%>';
var studentid = '<%=userId%>';
var courseIdList=[];
var courseIdMap={};
$(document).ready(function () {
	    initClassesMap();
	    initTable();
	    $("#query").on("click", function () {
		var studentid  = $("#studentid").val();
		var courseid  = $("#courseid").val();
		var semester  = $("#semester").val();
	        $.ajax({
	            type: "post",
	            url: "./querySxore",
	            data: {studentid:studentid,courseid:courseid,semester:semester},
	            dataType: "json",
	            success: function (info) {
	                if (info.status > 0) {
	                    $("#table").bootstrapTable("load", info.data);
	                } else {
	                    showTip(info.msg, false);
	                }
	            },
	            error: function (error) {
	                showTip("服务器异常，请联系管理员！", false);
	            }
	        })
	    });
	
		$("#courseidedit").on("change",function(){
			var self = this;
			$.each(courseIdList,function(index,item){
				courseIdMap[item.id]=item.name;
				if($(self).val()==item.id){
					$("#myModal #semesteredit").val(item.semester);
					$("#myModal #creditouredit").val(item.creditour);
					$("#myModal #classhouredit").val(item.classhour);
					$("#myModal #practicehouredit").val(item.practicehour);
					$("#myModal #remarkedit").val(item.remark);
				}
			})
		})

	    $("#insert").on("click", function () {
			        	$("#myModalLabel").html("选课");
		                $("#myModal #sxoreid").val();
		                $("#myModal input,#myModal select").val("").attr("disabled",true);;
		                $("#myModal #courseidedit").attr("disabled",false);
		                $("#myModal #stuid").val(studentid);
		        		$("#submitButton").attr("onclick", "submitForm('./chooseCourse')");
		                $("#myModal").modal("show");
	    });
	    $("#edit").on("click", function () {
	        var selections = $("#table").bootstrapTable("getSelections");
	        if (selections.length == 0) {
	            showTip("无选中内容", false);
	        } else {
	            if (selections.length > 1) {
	                showTip("最多选择一项", false);
	            } else {
		        	$("#myModalLabel").html("修改选课");
		            $("#myModal #stuid").val(studentid);
	                $("#myModal #studentnameedit").val(selections[0].studentname).attr("disabled",true);
	                $("#myModal #courseidedit").val(selections[0].courseid).attr("disabled",true);
					$("#courseidedit").change();
	                $("#submitButton").attr("onclick", "submitForm('./editChoosedCourse')");
	                $("#myModal").modal("show");
	            }
	        }
	    });
	    $("#delete").on("click", function () {
		        var selections = $("#table").bootstrapTable("getSelections");
		        if (selections.length == 0) {
		            showTip("无选中内容", false);
		        } else {
		            if (selections.length > 1) {
		                showTip("最多选择一项", false);
		            } else {
		                var cid = selections[0].id;
		                var studentid = selections[0].studentid;
		                $.ajax({
		                    type: "post",
		                    url: "./delChoosedCourse",
		                    data: {
		                        id: cid,studentid:studentid
		                    },
		                    dataType: "json",
		                    success: function (info) {
		                        if (info.status > 0) {
		                            showTip(info.msg, true);
		                        } else {
		                            showTip(info.msg, false);
		                        }
								$("#query").click();
		                    },
		                    error: function (error) {
		                        showTip(error.msg, false);
		                    }
		                })
		            }
		        }
		    })
	});
	//查询班级信息
	function initClassesMap() {
	    $.ajax({
	        type: "post",
	        dataType: "json",
	        url: "./queryCourse",
	        data: {},
	        success: function (info) {
	            if (info.status > 0) {
	                courseIdList = info.data;
					$.each(courseIdList,function(index,item){
						courseIdMap[item.id]=item.name;
						if(0==index){
							$("#courseidedit, #courseid").append($("<option>").val("").text("请选择"));
						}
						var option = $("<option>").val(item.id).text(item.name);
						$("#courseidedit, #courseid").append(option);
					})
	            }
	        },
	        error: function (error) {
	            showTip(error.msg, false);
	        }
	    })
	}
	function submitForm(url) {
	    $.ajax({
	        type: "post",
	        dataType: "json",
	        url: url,
	        data: $("#form1").serialize(),
		    beforeSend: function () {
		    	if(!$("#myModal #courseidedit").val()){
					showTip("请选择课程", false);
					return false;
		    	}
		    },
	        success: function (info) {
	            $("#myModal").modal("hide");
	            if (info.status > 0) {
	                showTip(info.msg, true);
	            } else {
	                showTip(info.msg, false);
	            }
				$("#query").click();
	        },
	        error: function (error) {
	            showTip(error.msg, false);
	        }
	    })
	}
	function initTable() {
	    $("#table").bootstrapTable("destroy");
	    $("#table").bootstrapTable({
	        dataType: "json",
	        url: "./querySxore2",
	        method: "get",
	        cache: false,
	        pagination: true,
	        columns: [{
	                field: "state",
	                checkbox: true,
	                align: "center",
	                valign: "middle"
	            }, {
	                field: "id",
	                title: "id",
	                visible: false
	            }, {
	                field: "studentid",
	                title: "学号",
	                visible: false
	            }, {
	                field: "studentname",
	                title: "姓名",
	                align: "center",
	                valign: "middle"
	            }, {
	                field: "courseid",
	                title: "课程",
	                align: "center",
	                valign: "middle",
					formatter : function(value, row, index) {
						if (courseIdMap[value]) {
							return courseIdMap[value];
						} else {
							return value;
						}
					}
	            }, {
	                field: "semester",
	                title: "学期",
	                align: "center",
	                valign: "middle",
					formatter: function (value, row, index) {
				           return value==1?"第一学期":(value==2?"第一学期":"");
				    }
	            }, {
	                field: "score1",
	                title: "一考成绩",
	                align: "center",
	                valign: "middle"
	            }, {
	                field: "score2",
	                title: "二考成绩",
	                align: "center",
	                valign: "middle"
	            }, {
	                field: "score3",
	                title: "三考成绩",
	                align: "center",
	                valign: "middle"
	            }],
	        onLoadSuccess: function (info) {
	            console.log(info);
	            if (info.status > 0) {
	                $("#table").bootstrapTable("load", info.data);
	            } else {
	                showTip(info.msg, false);
	            }
	        },
	        onLoadError: function (status) {
	            return false;
	        },
	    })
	};
</script>
</html>