/**
 * 
 */
$(document).ready(function(){
	$('.leaf-main-content').css('height', $(window).height()-51);
})
$(window).resize(function() {
	$('.leaf-main-content').css('height', $(window).height()-51);
});

function showTip(msg,flag){
	var color = "#4EA44E";//绿色
	if(flag) color = "#CC4532";//红色
	$('body').toast({
		top:"75px",
		isCenter:false,
		content: msg,
		duration:1500,
		background: color,
	});
}