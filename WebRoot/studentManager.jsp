<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<%
	String user = null;
	String userId = null;
	Short stype = null;
	if (session != null) {
		user = (String) session.getAttribute("user");
		userId = (String) session.getAttribute("userId");
		stype = (Short) session.getAttribute("stype");
	}
%>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- css -->
<link rel="stylesheet" href="./assets/toast/toast.css">
<link rel="stylesheet" href="./assets/bootstrap/dist/css/bootstrap.css">
<link rel="stylesheet"
	href="./assets/bootstrap-table/bootstrap-table.css">
<link rel="stylesheet" href="./assets/Font-Awesome/css/font-awesome.css">
<link rel="stylesheet" href="./assets/common.css">
<!-- js -->
<script type="text/javascript"
	src="./assets/jQuery/jquery-1.10.2.min.js"></script>
<script type="text/javascript"
	src="./assets/jQuery/jquery.form.js"></script>
<script type="text/javascript"
	src="./assets/bootstrap/dist/js/bootstrap.js"></script>
<script type="text/javascript"
	src="./assets/bootstrap-table/bootstrap-table.js"></script>
<script type="text/javascript"
	src="./assets/bootstrap-table/bootstrap-table-zh-CN.js"></script>
<script type="text/javascript" src="./assets/toast/toast.js"></script>
<script type="text/javascript" src="./assets/common.js"></script>
<title>用户管理</title>
</head>
<body>
	<div class="head">
		<h2 class="title">用户管理</h2>
	</div>
	<div class="container">
		<div class="col-lg-12" style="padding: 0 50px;">
			<div class="col-md-5" style="width: 30%;">
				<label for="modal" class="col-sm-2" style="width: 90px;">姓名</label>
				<div class="col-sm-8" style="padding: 0px;">
					<input type="text" class="form-control" id="name" placeholder="姓名"/>
				</div>
			</div>
			<div class="col-md-3">
				<label for="status" class="col-sm-3">班级</label>
				<div class="col-sm-6" style="padding: 0px">
					<select class="form-control" id="classesid">
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<label for="status" class="col-sm-3" style="width: 90px;">用户类型</label>
				<div class="col-sm-6" style="padding: 0px">
					<select class="form-control" id="stype">
						<option value="">请选择</option>
						<option value="0">普通用户</option>
						<option value="1">管理员</option>
					</select>
				</div>
			</div>
			<div class="col-md-1" style="width: 20%;">
				<button class="btn btn-primary" id="query" style="float: right;">查询</button>
			</div>
		</div>
		<div class="col-lg-12" style="padding: 0 50px;">
			<div class="col-md-12 toolbar"
				style="margin-top: 25px; margin-bottom: 5px;">
				<% if(stype==1){ %>
				<button class="btn btn-default" id="insert">
					<i class="fa fa-plus-square"></i> 新增
				</button>
				<button class="btn btn-default" id="edit"">
					<i class="fa fa-pencil-square"></i> 编辑
				</button>
					<button class="btn btn-default" id="delete">
						<i class="fa fa-times-circle"></i> 删除
					</button>
					<button class="btn btn-default" id="import">
						<i class="fa fa-upload"></i> 导入
					</button>
				<%} %>
			</div>
			<div class="col-md-12">
				<table class="table-bordered" id="table"></table>
			</div>
		</div>
	</div>

	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">班级修改</h4>
				</div>
				<div class="modal-body">
					<form onsubmit="return false" action="##" method="post" id="form1">
						<input type="hidden" class="form-control" id="studentid" name="id">
						<div class="form-group">
							<label for="nameedit">姓名</label> <input type="text"
								class="form-control" id="nameedit" name="name"
								placeholder="姓名" required>
						</div>
						<div class="form-group">
							<label for="nameedit">密码</label> <input type="password"
								class="form-control" id="pwdedit" name="pwd"
								placeholder="密码" required>
						</div>
						<div class="form-group">
							<label for="gradeedit">班级</label>
							<select class="form-control" id="classesidedit" name="classesid">
							</select> 
						</div>
						<div class="form-group">
							<label for="majoredit">用户类型</label>
							<select class="form-control" id="stypeedit" name="stype">
								<option value="">请选择</option>
								<option value="0">普通用户</option>
								<option value="1">管理员</option>
							</select> 
						</div>
						<div class="modal-footer">
							<input id="submitButton" type="button"
								onclick="submitForm('./editStudent')" class="btn btn-success"
								value="提交">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">关闭</button>
						</div>
					</form>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
	<div class="modal fade" id="myModal2" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel2" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel2">批量导入</h4>
				</div>
				<div class="modal-body">
					<form action="./uploadFile" method="post" enctype="multipart/form-data" method="post" id="fileForm" name="fileForm">
							<a href="./fileDownLoad" target="_blank">模板下载</a>
							<input type="file" name="file"/>
						<div class="modal-footer">
							<input id="submitButton" type="button" onclick="subimtBtn()" class="btn btn-success"
								value="提交">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">关闭</button>
						</div>
					</form>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
</body>

<script type="text/javascript">
var classIdList=[];
var classIdMap={};
$(document).ready(function () {
		initClassesMap();
	    initTable();
	    $("#query").on("click", function () {
		var name  = $("#name").val();
		var classesid  = $("#classesid").val();
		var stype  = $("#stype").val();
	        $.ajax({
	            type: "post",
	            url: "./queryStudent",
	            data: {name:name,classesid:classesid,stype:stype},
	            dataType: "json",
	            success: function (info) {
	                if (info.status > 0) {
	                    $("#table").bootstrapTable("load", info.data);
	                } else {
	                    showTip(info.msg, false);
	                }
	            },
	            error: function (error) {
	                showTip("服务器异常，请联系管理员！", false);
	            }
	        })
	    });
	    $("#insert").on("click", function () {
	        $("#myModalLabel").html("新增用户");
	        $("#myModal #studentid").val("");
	        $("#myModal #nameedit").val("");
	        $("#myModal #pwdedit").val("");
	        $("#myModal #classesidedit").val("");
	        $("#myModal #stypeedit").val("0");
	        $("#submitButton").attr("onclick", "submitForm('./addStudent')");
	        $("#myModal").modal("show");
	    });
	    $("#edit").on("click", function () {
	        var selections = $("#table").bootstrapTable("getSelections");
	        if (selections.length == 0) {
	            showTip("无选中内容", false);
	        } else {
	            if (selections.length > 1) {
	                showTip("最多选择一项", false);
	            } else {
		        	$("#myModalLabel").html("编辑用户");
	                $("#myModal #studentid").val(selections[0].id);
	                $("#myModal #nameedit").val(selections[0].name);
	                $("#myModal #pwdedit").val(selections[0].pwd);
	                $("#myModal #classesidedit").val(selections[0].classesid);
	                $("#myModal #stypeedit").val(selections[0].stype);
	                $("#submitButton").attr("onclick", "submitForm('./editStudent')");
	                $("#myModal").modal("show");
	            }
	        }
	    });
	    $("#import").on("click", function () {
	                $("#myModal2").modal("show");
	                $("#myModal2 input[name='file']").val("");
	    });
	    $("#delete").on("click", function () {
	        var selections = $("#table").bootstrapTable("getSelections");
	        if (selections.length == 0) {
	            showTip("无选中内容", false);
	        } else {
	            if (selections.length > 1) {
	                showTip("最多选择一项", false);
	            } else {
	                var classId = selections[0].id;
	                $.ajax({
	                    type: "post",
	                    url: "./delStudent",
	                    data: {
	                        id: classId
	                    },
	                    dataType: "json",
	                    success: function (info) {
	                        if (info.status > 0) {
	                            showTip(info.msg, true);
	                        } else {
	                            showTip(info.msg, false);
	                        }
							$("#query").click();
	                    },
	                    error: function (error) {
	                        showTip(error.msg, false);
	                    }
	                })
	            }
	        }
	    })
	});

	function subimtBtn() {
		var form = $("form[name=fileForm]");
		var options = {
			url : './uploadFile',
			type : 'post',
			success : function(info) {
					if (info.status > 0) {
				     	showTip(info.msg, true);
				    } else {
				    	showTip(info.msg, false);
				    }
					$("#myModal2").modal("hide");
					$("#query").click();
			}
		};
		form.ajaxSubmit(options);
	}
	//查询班级信息
	function initClassesMap() {
		$.ajax({
			type : "post",
			dataType : "json",
			url : "./selectClass",
			data : {},
			success : function(info) {
				if (info.status > 0) {
					classIdList = info.data;
					$.each(classIdList,
							function(index, item) {
								classIdMap[item.id] = item.name;
								if (0 == index) {
									$("#classesidedit, #classesid").append(
											$("<option>").val("").text("请选择"));
								}
								var option = $("<option>").val(item.id).text(
										item.name);
								$("#classesidedit, #classesid").append(option);
							})
				}
			},
			error : function(error) {
				showTip(error.msg, false);
			}
		})
	}

	function submitForm(url) {
		$.ajax({
			type : "post",
			dataType : "json",
			url : url,
			data : $("#form1").serialize(),
			beforeSend : function() {
				if (!$("#myModal #nameedit").val()) {
					showTip("姓名不能为空", false);
					return false;
				}
				if (!$("#myModal #pwdedit").val()) {
					showTip("密码不能为空", false);
					return false;
				}
				if (!$("#myModal #classesidedit").val()) {
					showTip("请选择班级", false);
					return false;
				}
				if (!$("#myModal #stypeedit").val()) {
					showTip("请选择用户类型", false);
					return false;
				}
			},
			success : function(info) {
				$("#myModal").modal("hide");
				if (info.status > 0) {
					showTip(info.msg, true);
				} else {
					showTip(info.msg, false);
				}
				$("#query").click();
			},
			error : function(error) {
				showTip(error.msg, false);
			}
		})
	}
	function initTable() {
		$("#table").bootstrapTable("destroy");
		$("#table").bootstrapTable({
			dataType : "json",
			url : "./queryStudent",
			method : "get",
			cache : false,
			pagination : true,
			columns : [ {
				field : "state",
				checkbox : true,
				align : "center",
				valign : "middle"
			}, {
				field : "id",
				title : "学号",
				visible : false
			}, {
				field : "name",
				title : "姓名",
				align : "center",
				valign : "middle"
			}, {
				field : "pwd",
				title : "密码",
				visible : false
			}, {
				field : "classesid",
				title : "班级",
				align : "center",
				valign : "middle",
				formatter : function(value, row, index) {
					if (classIdMap[value]) {
						return classIdMap[value];
					} else {
						return value;
					}
				}
			}, {
				field : "stype",
				title : "用户类型",
				align : "center",
				valign : "middle",
				formatter : function(value, row, index) {
					return value == 1 ? "管理员" : (value == 0 ? "普通用户" : "");
				}
			} ],
			onLoadSuccess : function(info) {
				console.log(info);
				if (info.status > 0) {
					$("#table").bootstrapTable("load", info.data);
				} else {
					showTip(info.msg, false);
				}
			},
			onLoadError : function(status) {
				return false;
			},
		})
	};
</script>
</html>