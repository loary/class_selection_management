<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<%
    String id = null;
    String msg = null;
    String status = null;
    if (session != null) {
        id = (String) session.getAttribute("user");
        msg = (String) session.getAttribute("msg");
        status = (String) session.getAttribute("status");
    }
%>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1,user-scalable=no" />
		<meta name="apple-mobile-web-app-capable" content="yes">
		<title>欢迎使用选课系统</title>
		<!-- css -->
		<link rel="stylesheet" href="./assets/toast/toast.css">
		<link rel="stylesheet" href="./assets/bootstrap/dist/css/bootstrap.css">
		<link rel="stylesheet" href="./assets/Font-Awesome/css/font-awesome.css">
		<!-- js -->
		<script type="text/javascript" src="./assets/jQuery/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="./assets/bootstrap/dist/js/bootstrap.js"></script>
		<script type="text/javascript" src="./assets/toast/toast.js"></script>
		<link rel="stylesheet" href="./css/common.css">
	</head>

	<body>
		<nav class="navbar navbar-default navbar-fixed-top bootsnav">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                <i class="fa fa-bars"></i>
            </button>
					<a class="navbar-brand-mc" href="index.html">
					<img src="./img/logo.png.png" class="logo"></a>
				</div>
				<div class="collapse navbar-collapse" id="navbar-menu">
				<%
	                if (id != null) {
	            %>
	
	            <ul class="nav navbar-nav navbar-right nav-actions">
	                <li><a><%=id%> 欢迎您!
	                </a></li>
	                <li><a id="change" href="#" data-toggle="modal"
	                       data-target="#myModal">修改密码</a></li>
	                <li><a href="./logout">退出登录</a></li>
	            </ul>
	            <%
	            } else {
	            %>
	           <ul class="nav navbar-nav navbar-right nav-actions">
	                <li><a href="./login.jsp">请登录！</a></li>
	            </ul>
	            <%
	                }
	            %>
				</div>
			</div>
		</nav>
		<div class="leaf-nav">
			<div class="all-nav">
				<div class="home">
					<a class="home-logo" href="./index">
						<h1><i class="fa fa-leaf"></i></h1> 首页
					</a>
				</div>
				<div class="item">
					<div class="product">
						<a>
							<h1><i class="fa fa-cog fa-fw"></i></h1> 系统管理
						</a>
					</div>
					<div class="product-wrap pos01">
						<h2>
				          <i class="fa fa-cog"></i>  系统管理
				        </h2>
						<div class="gdt" style="height:90%">
							<ul>
								<li><a  href="./managerClass" target="mainF">班级管理</a></li>
								<li><a  href="./courseManager" target="mainF">课程管理</a></li>
								<li><a href="./managerStudent" target="mainF">用户管理</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="product">
						<a>
							<h1><i class="fa fa-users"></i></h1> 用户管理 
						</a>
					</div>
					<div class="product-wrap pos02">
						<h2>  <i class="fa fa-users"></i> 用户管理 </h2>
						<div class="gdt" style="height:90%">
							<ul>
								<li><a href="./managerStudent" target="mainF">用户管理</a></li>
								<li><a href="./managerStudent" target="mainF">用户查询</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="product">
						<a>
							<h1><i class="fa fa-book fa-fw"></i></h1> 课程管理
						</a>
					</div>
					<div class="product-wrap pos03">
						<h2><i class="fa fa-book"></i> 课程管理</h2>
						<div class="gdt" style="height:90%">
							<ul>
								<li><a href="./courseManager" target="mainF">课程管理</a></li>
								<li><a href="./courseChoose" target="mainF">选课管理</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="product">
						<a >
							<h1><i class="fa fa-ge"></i></h1> 成绩管理
						</a>
					</div>
					<div class="product-wrap pos04">
						<h2> <i class="fa fa-ge"></i> 成绩管理</h2>
						<div class="gdt" style="height:90%">
							<ul>
								<li><a href="./managerSxore" target="mainF">成绩管理</a></li>
								<li><a href="./printSxore" target="mainF">绩点查询</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="product">
						<a >
							<h1><i class="fa fa-info-circle"></i></h1> 其他
						</a>
					</div>
					<div class="product-wrap pos04">
						<h2> <i class="fa fa-info-circle"></i> 其他</h2>
						<div class="gdt" style="height:90%">
							<ul>
								<li><a id="showInfo">查看个人详细资料</a></li>
								<li><a href="./logout">退出系统</a></li>
								<!-- <li><a href="">查看课程详细信息</a></li> -->
							</ul>
						</div>
					</div>
				</div>

			</div>
		</div>
		<!--end leaf-nav-->
		<div class="leaf-main-content-wrap">
			<div class="leaf-main-content">
				<iframe frameborder="0" name="mainF" src="./content.jsp" scrolling="auto" style="height: 100%; width: 100%;"></iframe>
			</div>
		</div>
		
<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        修改密码
                    </h4>
                </div>
                <div class="modal-body">
                    <form action="./changePwd" method="post">
                        <div class="form-group">
                            <label for="old">旧密码</label>
                            <input type="password" class="form-control" id="old" name="old" placeholder="旧密码" required>
                        </div>
                        <div class="form-group">
                            <label for="newpwd">新密码</label>
                            <input type="password" class="form-control" id="newpwd" name="newpwd" placeholder="新密码" required>
                        </div>
                        <div class="form-group">
                            <label for="newagain">确认新密码</label>
                            <input type="password" class="form-control" id="newagain" name="newagain" placeholder="再次输入新密码" required>
                        </div>
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-success" value="提交">
                            <button type="button" class="btn btn-default"
                                    data-dismiss="modal">关闭
                            </button>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </div>
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel2" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">
                      个人信息
                    </h4>
                </div>
                <div class="modal-body">
                    <form action="./changePwd" method="post">
                        <div class="form-group">
                            <label for="name">姓名：</label><span id="name"></span>
                        </div>
                        <div class="form-group">
                            <label for="classesid">班级：</label><span id="classesid"></span>
                        </div>
                        <div class="form-group">
                            <label for="stype">角色类型：</label><span id="stype"></span>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default"
                                    data-dismiss="modal">关闭
                            </button>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </div>
		<script type="text/javascript" src="js/leaf.js"></script>
		<script type="text/javascript" src="js/index.js"></script>
		
		<script>
			var _msg = "<%=msg%>"
			_msg = "<%=msg%>"=="null"?"请登录":_msg;
			<%if (msg != null&&"1".equals(status)) {%>
				showTip(_msg);
			<%session.setAttribute("msg", null);
			} else {%>
				showTip(_msg,true);
				window.location = "./login.jsp";
			<%session.setAttribute("msg", null);}%>
			
			$(document).ready(function () {
				    $("#showInfo").on("click", function () {
				        $.ajax({
				            type: "post",
				            url: "./queryStudentInfo",
				            data: {},
				            dataType: "json",
				            success: function (info) {
				                if (info.status > 0) {
								$.each(info.data,function(index,item){
									$("#myModal2 #name").html(item.name);
									$("#myModal2 #stype").html(item.stype==1?"管理员":(item.stype==0?"普通用户":""));
									$("#myModal2 #classesid").html(item.classname);
									$("#myModal2").modal("show");
								})
				                } else {
				                    showTip(info.msg, false);
				                }
				            },
				            error: function (error) {
				                showTip("服务器异常，请联系管理员！", false);
				            }
				        })
				    });
				    });
				 
		</script>
	</body>
</html>
