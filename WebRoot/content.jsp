<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>content</title>
<style>
body {
	background: url(./img/homepage-bg.jpg) no-repeat;
	background-position: center;
	background-size: cover;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	min-height: 425px;
}
.banner-info {
    text-align: center;
    width: 100%;
    padding: 180px 0 0 0;
}
.banner-info h3{
	color: #fff;
	font-size: 52px;
	font-weight: bold;
	text-shadow: 1px 1px 1px rgba(0,0,0,0.1);
	margin: 0;
	font-family: 'Cinzel', serif;
	padding: 60px 0 20px 0;
}
.banner-info p {
	color: #fff;
	text-shadow: 1px 1px 1px rgba(0,0,0,0.1);
	font-size: 34px;
	font-weight: normal;
	position: relative;
	margin: 0;
}
.banner-info span.line1 {
    width: 484px;
    height: 1px;
    background-color: #fff;
    display: block;
    margin: 0 auto;
}

.banner-info span.line2 {
    width: 484px;
    height: 1px;
    display: block;
    background-color: #fff;
    margin: 0 auto;
}
</style>
</head>
<body>
	<div class="banner-info">
		<h3>选课系统</h3>
		<span class="line1"></span>
		<p>欢迎使用学生选课系统.</p>
		<span class="line2"></span>
	</div>
</body>
</html>