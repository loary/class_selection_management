<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<%
	String user = null;
	String userId = null;
	Short stype = null;
	if (session != null) {
		user = (String) session.getAttribute("user");
		userId = (String) session.getAttribute("userId");
		stype = (Short) session.getAttribute("stype");
	}
%>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- css -->
<link rel="stylesheet" href="./assets/toast/toast.css">
<link rel="stylesheet" href="./assets/bootstrap/dist/css/bootstrap.css">
<link rel="stylesheet"
	href="./assets/bootstrap-table/bootstrap-table.css">
<link rel="stylesheet" href="./assets/Font-Awesome/css/font-awesome.css">
<link rel="stylesheet" href="./assets/common.css">
<!-- js -->
<script type="text/javascript"
	src="./assets/jQuery/jquery-1.10.2.min.js"></script>
<script type="text/javascript"
	src="./assets/bootstrap/dist/js/bootstrap.js"></script>
<script type="text/javascript"
	src="./assets/bootstrap-table/bootstrap-table.js"></script>
<script type="text/javascript"
	src="./assets/bootstrap-table/bootstrap-table-zh-CN.js"></script>
<script type="text/javascript" src="./assets/toast/toast.js"></script>
<script type="text/javascript" src="./assets/common.js"></script>
<title>成绩管理</title>
</head>
<body>
	<div class="head">
		<h2 class="title">成绩管理</h2>
	</div>
	<div class="container">
		<div class="col-lg-12" style="padding: 0 50px;">
			<div class="col-md-5" style="width: 30%;">
				<label for="modal" class="col-sm-2" style="width: 90px;">学号</label>
				<div class="col-sm-8" style="padding: 0px;">
					<input type="text" class="form-control" id="studentid" placeholder="学号"/>
				</div>
			</div>
			<div class="col-md-3">
				<label for="status" class="col-sm-3">课程</label>
				<div class="col-sm-6" style="padding: 0px">
					<select class="form-control" id="courseid">
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<label for="status" class="col-sm-3" style="width: 90px;">学期</label>
				<div class="col-sm-6" style="padding: 0px">
					<select class="form-control" id="semester">
						<option value="">请选择</option>
						<option value="1">第一学期</option>
						<option value="2">第二学期</option>
					</select>
				</div>
			</div>
			<div class="col-md-1" style="width: 20%;">
				<button class="btn btn-primary" id="query" style="float: right;">查询</button>
			</div>
		</div>
		<div class="col-lg-12" style="padding: 0 50px;">
			<div class="col-md-12 toolbar"
				style="margin-top: 25px; margin-bottom: 5px;">
				<% if(stype==1){ %>
				<button class="btn btn-default" id="insert">
					<i class="fa fa-plus-square"></i> 成绩录入
				</button>
				<button class="btn btn-default" id="edit"">
					<i class="fa fa-pencil-square"></i> 成绩修改
				</button>
				<%} %>
			</div>
			<div class="col-md-12">
				<table class="table-bordered" id="table"></table>
			</div>
		</div>
	</div>

	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">成绩录入</h4>
				</div>
				<div class="modal-body">
					<form onsubmit="return false" action="##" method="post" id="form1">
						<input type="hidden" class="form-control" id="sxoreid" name="id">
						<input type="hidden" class="form-control" id="stuid" name="studentid">
						<div class="form-group">
							<label for="nameedit">姓名</label> <input type="text"
								class="form-control" id="studentnameedit" name="studentname"
								placeholder="姓名" required>
						</div>
						<div class="form-group">
							<label for="gradeedit">课程</label>
							<select class="form-control" id="courseidedit" name="courseid">
							</select> 
						</div>
						<div class="form-group">
							<label for="majoredit">学期</label> <select class="form-control"
								id="semesteredit" name="semester">
								<option value="">请选择</option>
								<option value="1">第一学期</option>
								<option value="2">第二学期</option>
							</select>
						</div>
						<div class="form-group">
							<label for="majoredit">成绩</label>
							<input type="text" class="form-control" id="score1edit" name="score1" placeholder="一考成绩" >
							<input type="text" class="form-control" id="score2edit" name="score2" placeholder="二考成绩" >
							<input type="text" class="form-control" id="score3edit" name="score3" placeholder="三考成绩" >
						</div>
						<div class="modal-footer">
							<input id="submitButton" type="button"
								onclick="submitForm('./editSxore')" class="btn btn-success"
								value="提交">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">关闭</button>
						</div>
					</form>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
</body>

<script type="text/javascript">
var courseIdList=[];
var courseIdMap={};
$(document).ready(function () {
	    initClassesMap();
	    initTable();
	    $("#query").on("click", function () {
		var studentid  = $("#studentid").val();
		var courseid  = $("#courseid").val();
		var semester  = $("#semester").val();
	        $.ajax({
	            type: "post",
	            url: "./querySxore",
	            data: {studentid:studentid,courseid:courseid,semester:semester},
	            dataType: "json",
	            success: function (info) {
	                if (info.status > 0) {
	                    $("#table").bootstrapTable("load", info.data);
	                } else {
	                    showTip(info.msg, false);
	                }
	            },
	            error: function (error) {
	                showTip("服务器异常，请联系管理员！", false);
	            }
	        })
	    });

	    $("#insert").on("click", function () {
	        var selections = $("#table").bootstrapTable("getSelections");
	        if (selections.length == 0) {
	            showTip("无选中内容", false);
	        } else {
	            if (selections.length > 1) {
	                showTip("最多选择一项", false);
	            } else {
					$("#myModal #score1edit,#myModal #score2edit,#myModal #score3edit").attr("disabled",false);
					<% if(stype!=1){ %>
							selections[0].score1?($("#myModal #score1edit").attr("disabled",true)):"";
							selections[0].score2?($("#myModal #score2edit").attr("disabled",true)):"";
							selections[0].score3?($("#myModal #score3edit").attr("disabled",true)):"";
							if(selections[0].score1&&selections[0].score2&&selections[0].score3){
								showTip("已提交数据不允许修改！", false);
							}else{
					        	$("#myModalLabel").html("成绩录入");
				                $("#myModal #sxoreid").val(selections[0].id);
				                $("#myModal #stuid").val(selections[0].studentid);
				                $("#myModal #score1edit").val(selections[0].score1||"");
				                $("#myModal #score2edit").val(selections[0].score2||"");
				                $("#myModal #score3edit").val(selections[0].score3||"");
				                $("#myModal #studentnameedit").val(selections[0].studentname).attr("disabled",true);
				                $("#myModal #semesteredit").val(selections[0].semester).attr("disabled",true);
				                $("#myModal #courseidedit").val(selections[0].courseid).attr("disabled",true);
				        		$("#submitButton").attr("onclick", "submitForm('./editSxore')");
				                $("#myModal").modal("show");
							}
					<%}  else{%>
			        	$("#myModalLabel").html("成绩录入");
		                $("#myModal #sxoreid").val(selections[0].id);
		                $("#myModal #stuid").val(selections[0].studentid);
		                $("#myModal #score1edit").val(selections[0].score1||"");
		                $("#myModal #score2edit").val(selections[0].score2||"");
		                $("#myModal #score3edit").val(selections[0].score3||"");
		                $("#myModal #studentnameedit").val(selections[0].studentname).attr("disabled",true);
		                $("#myModal #semesteredit").val(selections[0].semester).attr("disabled",true);
		                $("#myModal #courseidedit").val(selections[0].courseid).attr("disabled",true);
		        		$("#submitButton").attr("onclick", "submitForm('./editSxore')");
		                $("#myModal").modal("show");
					<%}%>
	            }
	        }
	    });
	    $("#edit").on("click", function () {
	        var selections = $("#table").bootstrapTable("getSelections");
	        if (selections.length == 0) {
	            showTip("无选中内容", false);
	        } else {
	            if (selections.length > 1) {
	                showTip("最多选择一项", false);
	            } else {
		        	$("#myModalLabel").html("成绩修改");
	                $("#myModal #score1edit").val(selections[0].score1||"");
	                $("#myModal #score2edit").val(selections[0].score2||"");
	                $("#myModal #score3edit").val(selections[0].score3||"");
	                $("#myModal #studentnameedit").val(selections[0].studentname).attr("disabled",true);
	                $("#myModal #semesteredit").val(selections[0].semester).attr("disabled",true);
	                $("#myModal #courseidedit").val(selections[0].courseid).attr("disabled",true);
	                $("#submitButton").attr("onclick", "submitForm('./editSxore')");
	                $("#myModal").modal("show");
	            }
	        }
	    });
	});
	 
	//查询班级信息
	function initClassesMap() {
	    $.ajax({
	        type: "post",
	        dataType: "json",
	        url: "./queryCourse",
	        data: {},
	        success: function (info) {
	            if (info.status > 0) {
	                courseIdList = info.data;
					$.each(courseIdList,function(index,item){
						courseIdMap[item.id]=item.name;
						if(0==index){
							$("#courseidedit, #courseid").append($("<option>").val("").text("请选择"));
						}
						var option = $("<option>").val(item.id).text(item.name);
						$("#courseidedit, #courseid").append(option);
					})
	            }
	        },
	        error: function (error) {
	            showTip(error.msg, false);
	        }
	    })
	}
	function submitForm(url) {
	    $.ajax({
	        type: "post",
	        dataType: "json",
	        url: url,
	        data: $("#form1").serialize(),
		    beforeSend: function () {
		    	var reg = new RegExp("^[0-9]+(.[0-9]{0,2})?$");//正实数
	    		if($("#myModal #score1edit").val()){
			    	if(!reg.test($("#myModal #score1edit").val())){
						showTip("成绩无效", false);
						return false;
			    	}
		    	}
	    		if($("#myModal #score2edit").val()){
			    	if(!reg.test($("#myModal #score1edit").val())){
						showTip("成绩无效", false);
						return false;
			    	}
		    	}
	    		if($("#myModal #score3edit").val()){
			    	if(!reg.test($("#myModal #score1edit").val())){
						showTip("成绩无效", false);
						return false;
			    	}
		    	}
		    },
	        success: function (info) {
	            $("#myModal").modal("hide");
	            if (info.status > 0) {
	                showTip(info.msg, true);
	            } else {
	                showTip(info.msg, false);
	            }
				$("#query").click();
	        },
	        error: function (error) {
	            showTip(error.msg, false);
	        }
	    })
	}
	function initTable() {
	    $("#table").bootstrapTable("destroy");
	    $("#table").bootstrapTable({
	        dataType: "json",
	        url: "./querySxore",
	        method: "get",
	        cache: false,
	        pagination: true,
	        columns: [{
	                field: "state",
	                checkbox: true,
	                align: "center",
	                valign: "middle"
	            }, {
	                field: "id",
	                title: "id",
	                visible: false
	            }, {
	                field: "studentid",
	                title: "学号",
	                visible: false
	            }, {
	                field: "studentname",
	                title: "姓名",
	                align: "center",
	                valign: "middle"
	            }, {
	                field: "courseid",
	                title: "课程",
	                align: "center",
	                valign: "middle",
					formatter: function (value, row, index) {
						if(courseIdMap[value]){
							return courseIdMap[value];
						}else{
							return value;
						}
				     }
	            }, {
	                field: "semester",
	                title: "学期",
	                align: "center",
	                valign: "middle",
					formatter: function (value, row, index) {
				           return value==1?"第一学期":(value==2?"第一学期":"");
				    }
	            }, {
	                field: "score1",
	                title: "一考成绩",
	                align: "center",
	                valign: "middle"
	            }, {
	                field: "score2",
	                title: "二考成绩",
	                align: "center",
	                valign: "middle"
	            }, {
	                field: "score3",
	                title: "三考成绩",
	                align: "center",
	                valign: "middle"
	            }],
	        onLoadSuccess: function (info) {
	            console.log(info);
	            if (info.status > 0) {
	                $("#table").bootstrapTable("load", info.data);
	            } else {
	                showTip(info.msg, false);
	            }
	        },
	        onLoadError: function (status) {
	            return false;
	        },
	    })
	};
</script>
</html>