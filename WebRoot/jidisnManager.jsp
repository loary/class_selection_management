<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<%
	String user = null;
	String userId = null;
	Short stype = null;
	if (session != null) {
		user = (String) session.getAttribute("user");
		userId = (String) session.getAttribute("userId");
		stype = (Short) session.getAttribute("stype");
	}
%>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- css -->
<link rel="stylesheet" href="./assets/toast/toast.css">
<link rel="stylesheet" href="./assets/bootstrap/dist/css/bootstrap.css">
<link rel="stylesheet"
	href="./assets/bootstrap-table/bootstrap-table.css">
<link rel="stylesheet" href="./assets/Font-Awesome/css/font-awesome.css">
<link rel="stylesheet" href="./assets/common.css">
<!-- js -->
<script type="text/javascript"
	src="./assets/jQuery/jquery-1.10.2.min.js"></script>
<script type="text/javascript"
	src="./assets/bootstrap/dist/js/bootstrap.js"></script>
<script type="text/javascript"
	src="./assets/bootstrap-table/bootstrap-table.js"></script>
<script type="text/javascript"
	src="./assets/bootstrap-table/bootstrap-table-zh-CN.js"></script>
<script type="text/javascript"
	src="./assets/jQuery.print.min.js"></script>
<script type="text/javascript" src="./assets/toast/toast.js"></script>
<script type="text/javascript" src="./assets/common.js"></script>
<title>绩点查询</title>
</head>
<body>
	<div class="head">
		<h2 class="title">绩点查询</h2>
	</div>
	<div class="container">
		<div class="col-lg-12" style="padding: 0 50px;">
			<div class="col-md-5" style="width: 30%;">
				<label for="modal" class="col-sm-2" style="width: 90px;">学号</label>
				<div class="col-sm-8" style="padding: 0px;">
					<input type="text" class="form-control" id="studentid" name="studentid" placeholder="学号"/>
				</div>
			</div>
			<div class="col-md-3">
				<label for="status" class="col-sm-3" style="width: 90px;">学期</label>
				<div class="col-sm-6" style="padding: 0px">
					<select class="form-control" id="semester" name="semester">
						<option value="">请选择</option>
						<option value="1">第一学期</option>
						<option value="2">第二学期</option>
					</select>
				</div>
			</div>
			<div class="col-md-1" style="width: 20%;">
				<button class="btn btn-primary" id="query" style="float: right;">查询</button>
			</div>
		</div>
		<div class="col-lg-12" style="padding: 0 50px;">
			<div class="col-md-12 toolbar"
				style="margin-top: 25px; margin-bottom: 5px;">
			</div>
			<div class="col-md-12">
				<table class="table-bordered" id="table"></table>
			</div>
			<div class="col-md-12" style="text-align: center;">
				<% if(stype==1){ %>
				<button class="btn btn-default" id="print"">
					<i class="fa fa-print"></i> 打印
				</button>
				<%} %>
			</div>
		</div>
	</div>
</body>

<script type="text/javascript">
var courseIdList=[];
var courseIdMap={};
$(document).ready(function () {
	    initClassesMap();
	    initTable();
	    $("#query").on("click", function () {
			var studentid  = $("#studentid").val();
			var semester  = $("#semester").val();
	        $.ajax({
	            type: "post",
	            url: "./querySxore2",
	            data: {semester:semester,studentid:studentid},
	            dataType: "json",
	            success: function (info) {
	                if (info.status > 0) {
	                    $("#table").bootstrapTable("load", info.data);
	                } else {
	                    showTip(info.msg, false);
	                }
	            },
	            error: function (error) {
	                showTip("服务器异常，请联系管理员！", false);
	            }
	        })
	    });
	    $("#print").on("click", function () {
			$("#table").print();
	    });
	});
	 
	//查询班级信息
	function initClassesMap() {
	    $.ajax({
	        type: "post",
	        dataType: "json",
	        url: "./queryCourse",
	        data: {},
	        success: function (info) {
	            if (info.status > 0) {
	                courseIdList = info.data;
					$.each(courseIdList,function(index,item){
						courseIdMap[item.id]=item.name;
						if(0==index){
							$("#courseidedit, #courseid").append($("<option>").val("").text("请选择"));
						}
						var option = $("<option>").val(item.id).text(item.name);
						$("#courseidedit, #courseid").append(option);
					})
	            }
	        },
	        error: function (error) {
	            showTip(error.msg, false);
	        }
	    })
	}
	function initTable() {
	    $("#table").bootstrapTable("destroy");
	    $("#table").bootstrapTable({
	        dataType: "json",
	        url: "./querySxore2",
	        method: "get",
	        cache: false,
	        pagination: true,
			showFooter: true,
	        columns: [{
	                field: "id",
	                title: "id",
	                visible: false
	            }, {
	                field: "studentid",
	                title: "学号",
	                align: "center",
	                valign: "middle"
	            }, {
	                field: "studentname",
	                title: "姓名",
	                align: "center",
	                valign: "middle"
	            }, {
	                field: "courseid",
	                title: "课程",
	                align: "center",
	                valign: "middle",
					formatter: function (value, row, index) {
						if(courseIdMap[value]){
							return courseIdMap[value];
						}else{
							return value;
						}
				     }
	            }, {
	                field: "semester",
	                title: "学期",
	                align: "center",
	                valign: "middle",
					formatter: function (value, row, index) {
				        return value==1?"第一学期":(value==2?"第一学期":"");
				 }
	            }, {
	                field: "score1",
	                title: "一考成绩",
	                align: "center",
	                valign: "middle"
	            }, {
	                field: "score2",
	                title: "二考成绩",
	                align: "center",
	                valign: "middle"
	            }, {
	                field: "score3",
	                title: "三考成绩",
	                align: "center",
	                valign: "middle"
	            }, {
	                field: "creditour",
	                title: "学分",
	                align: "center",
	                valign: "middle",
					footerFormatter:function(data){
					    field = this.field;
					    return data.reduce(function(sum, row) { 
					        return sum + (+row[field]);
					    }, 0);
				    }
	            }],
	        onLoadSuccess: function (info) {
	            console.log(info);
	            if (info.status > 0) {
	                $("#table").bootstrapTable("load", info.data);
	            } else {
	                showTip(info.msg, false);
	            }
	        },
	        onLoadError: function (status) {
	            return false;
	        },
	    })
	};
</script>
</html>