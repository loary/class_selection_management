<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<%
	String user = null;
	String userId = null;
	Short stype = null;
	if (session != null) {
		user = (String) session.getAttribute("user");
		userId = (String) session.getAttribute("userId");
		stype = (Short) session.getAttribute("stype");
	}
%>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- css -->
<link rel="stylesheet" href="./assets/toast/toast.css">
<link rel="stylesheet" href="./assets/bootstrap/dist/css/bootstrap.css">
<link rel="stylesheet"
	href="./assets/bootstrap-table/bootstrap-table.css">
<link rel="stylesheet" href="./assets/Font-Awesome/css/font-awesome.css">
<link rel="stylesheet" href="./assets/common.css">
<!-- js -->
<script type="text/javascript"
	src="./assets/jQuery/jquery-1.10.2.min.js"></script>
<script type="text/javascript"
	src="./assets/bootstrap/dist/js/bootstrap.js"></script>
<script type="text/javascript"
	src="./assets/bootstrap-table/bootstrap-table.js"></script>
<script type="text/javascript"
	src="./assets/bootstrap-table/bootstrap-table-zh-CN.js"></script>
<script type="text/javascript" src="./assets/toast/toast.js"></script>
<script type="text/javascript" src="./assets/common.js"></script>
<title>课程管理</title>
</head>
<body>
	<div class="head">
		<h2 class="title">课程管理</h2>
	</div>
	<div class="container">
		<div class="col-lg-12" style="padding: 0 50px;">
			<div class="col-md-5" style="width: 30%;">
				<label for="modal" class="col-sm-2" style="width: 90px;">课程名称</label>
				<div class="col-sm-8" style="padding: 0px;">
					<input type="text" class="form-control" id="name" placeholder="课程名称"/>
				</div>
			</div>
			<div class="col-md-3">
				<label for="status" class="col-sm-3" style="width: 90px;">学期</label>
				<div class="col-sm-6" style="padding: 0px">
					<select class="form-control" id="semester">
						<option value="">请选择</option>
						<option value="1">第一学期</option>
						<option value="2">第二学期</option>
					</select>
				</div>
			</div>
			<div class="col-md-1" style="width: 20%;">
				<button class="btn btn-primary" id="query" style="float: right;">查询</button>
			</div>
		</div>
		<div class="col-lg-12" style="padding: 0 50px;">
			<div class="col-md-12 toolbar"
				style="margin-top: 25px; margin-bottom: 5px;">
				<% if(stype==1){ %>
				<button class="btn btn-default" id="insert">
					<i class="fa fa-plus-square"></i> 新增
				</button>
				<button class="btn btn-default" id="edit"">
					<i class="fa fa-pencil-square"></i> 编辑
				</button>
					<button class="btn btn-default" id="delete">
						<i class="fa fa-times-circle"></i> 删除
					</button>
				<%} %>
			</div>
			<div class="col-md-12">
				<table class="table-bordered" id="table"></table>
			</div>
		</div>
	</div>

	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">班级修改</h4>
				</div>
				<div class="modal-body">
					<form onsubmit="return false" action="##" method="post" id="form1">
						<input type="hidden" class="form-control" id="courseid" name="id">
						<div class="form-group">
							<label for="nameedit">姓名</label> <input type="text"
								class="form-control" id="nameedit" name="name"
								placeholder="姓名" required>
						</div>
						<div class="form-group">
							<label for="nameedit">学分</label> <input type="text"
								class="form-control" id="creditouredit" name="creditour"
								placeholder="学分" required>
						</div>
						<div class="form-group">
							<label for="nameedit">学期</label> 
							<select class="form-control" id="semesteredit" name="semester">
								<option value="">请选择</option>
								<option value="1">第一学期</option>
								<option value="2">第二学期</option>
							</select>
						</div>
						<div class="form-group">
							<label for="gradeedit">讲授学时</label>
							<input type="text"
								class="form-control" id="classhouredit" name="classhour"
								placeholder="讲授学时" required>
						</div>
						<div class="form-group">
							<label for="gradeedit">实验学时</label>
							<input type="text"
								class="form-control" id="practicehouredit" name="practicehour"
								placeholder="实验学时" required>
						</div>
						<div class="form-group">
							<label for="majoredit">备注</label>
							<input type="text"
								class="form-control"id="remarkedit" name="remark"
								placeholder="备注" >
						</div>
						<div class="modal-footer">
							<input id="submitButton" type="button"
								onclick="submitForm('./editStudent')" class="btn btn-success"
								value="提交">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">关闭</button>
						</div>
					</form>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
</body>

<script type="text/javascript">
$(document).ready(function () {
	    initTable();
	    $("#query").on("click", function () {
		var name  = $("#name").val();
		var semester  = $("#semester").val();
	        $.ajax({
	            type: "post",
	            url: "./querySelectCourse",
	            data: {name:name,semester:semester},
	            dataType: "json",
	            success: function (info) {
	                if (info.status > 0) {
	                    $("#table").bootstrapTable("load", info.data);
	                } else {
	                    showTip(info.msg, false);
	                }
	            },
	            error: function (error) {
	                showTip("服务器异常，请联系管理员！", false);
	            }
	        })
	    });
	    $("#insert").on("click", function () {
	        $("#myModalLabel").html("新增课程");
	        $("#myModal input").val("");
	        $("#myModal select").val("");
	        $("#submitButton").attr("onclick", "submitForm('./addCourse')");
	        $("#myModal").modal("show");
	    });
	    $("#edit").on("click", function () {
	        var selections = $("#table").bootstrapTable("getSelections");
	        if (selections.length == 0) {
	            showTip("无选中内容", false);
	        } else {
	            if (selections.length > 1) {
	                showTip("最多选择一项", false);
	            } else {
		        	$("#myModalLabel").html("编辑课程");
	                $("#myModal #courseid").val(selections[0].id);
	                $("#myModal #nameedit").val(selections[0].name);
	                $("#myModal #creditouredit").val(selections[0].creditour);
	                $("#myModal #semesteredit").val(selections[0].semester);
	                $("#myModal #classhouredit").val(selections[0].classhour);
	                $("#myModal #practicehouredit").val(selections[0].practicehour);
	                $("#myModal #remarkedit").val(selections[0].remark);
	                $("#submitButton").attr("onclick", "submitForm('./editCourse')");
	                $("#myModal").modal("show");
	            }
	        }
	    });
	    $("#delete").on("click", function () {
	        var selections = $("#table").bootstrapTable("getSelections");
	        if (selections.length == 0) {
	            showTip("无选中内容", false);
	        } else {
	            if (selections.length > 1) {
	                showTip("最多选择一项", false);
	            } else {
	                var courseId = selections[0].id;
	                $.ajax({
	                    type: "post",
	                    url: "./delCourse",
	                    data: {
	                        id: courseId
	                    },
	                    dataType: "json",
	                    success: function (info) {
	                        if (info.status > 0) {
	                            showTip(info.msg, true);
	                        } else {
	                            showTip(info.msg, false);
	                        }
							$("#query").click();
	                    },
	                    error: function (error) {
	                        showTip(error.msg, false);
	                    }
	                })
	            }
	        }
	    })
	});
	 
	function submitForm(url) {
	    $.ajax({
	        type: "post",
	        dataType: "json",
	        url: url,
	        data: $("#form1").serialize(),
		    beforeSend: function () {
		    	if(!$("#myModal #nameedit").val()){
					showTip("课程名称不能为空", false);
					return false;
		    	}
		    	if(!$("#myModal #creditouredit").val()){
					showTip("学分不能为空", false);
					return false;
		    	}
		    	var reg = new RegExp("^[0-9]+(.[0-9]{0,2})?$");//正实数
		    	if(!reg.test($("#myModal #creditouredit").val())){
					showTip("学分无效", false);
					return false;
		    	}
		    	if(!$("#myModal #semesteredit").val()){
					showTip("请选择开课学期", false);
					return false;
		    	}
		    	if(!reg.test($("#myModal #classhouredit").val())){
					showTip("讲授学时无效", false);
					return false;
		    	}
		    	if(!reg.test($("#myModal #practicehouredit").val())){
					showTip("实验学时无效", false);
					return false;
		    	}
		    },
	        success: function (info) {
	            $("#myModal").modal("hide");
	            if (info.status > 0) {
	                showTip(info.msg, true);
	            } else {
	                showTip(info.msg, false);
	            }
				$("#query").click();
	        },
	        error: function (error) {
	            showTip(error.msg, false);
	        }
	    })
	}
	function initTable() {
	    $("#table").bootstrapTable("destroy");
	    $("#table").bootstrapTable({
	        dataType: "json",
	        url: "./querySelectCourse",
	        method: "get",
	        cache: false,
	        pagination: true,
	        columns: [{
	                field: "state",
	                checkbox: true,
	                align: "center",
	                valign: "middle"
	            }, {
	                field: "id",
	                title: "课程编号",
	                visible: false
	            }, {
	                field: "name",
	                title: "课程名称",
	                align: "center",
	                valign: "middle"
	            },{
		            field: "creditour",
		            title: "学分",
		            align: "center",
		            valign: "middle"
		         }, {
	                field: "semester",
	                title: "开课学期",
	                align: "center",
	                valign: "middle",
    				formatter: function (value, row, index) {
	   	                 return value==1?"第一学期":(value==2?"第一学期":"");
    	             }
	            }, {
	                field: "classhour",
	                title: "讲授学时",
	                align: "center",
	                valign: "middle"
	            },{
	                field: "practicehour",
	                title: "实验学时",
	                align: "center",
	                valign: "middle"
	            },{
	                field: "remark",
	                title: "备注",
	                align: "center",
	                valign: "middle"
	            }
				],
	        onLoadSuccess: function (info) {
	            console.log(info);
	            if (info.status > 0) {
	                $("#table").bootstrapTable("load", info.data);
	            } else {
	                showTip(info.msg, false);
	            }
	        },
	        onLoadError: function (status) {
	            return false;
	        },
	    })
	};
</script>
</html>