-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.5.8 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win32
-- HeidiSQL 版本:                  9.3.0.5077
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- 导出 test01 的数据库结构
DROP DATABASE IF EXISTS `test01`;
CREATE DATABASE IF NOT EXISTS `test01` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `test01`;

-- 导出  表 test01.admin 结构
DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `id` varchar(20) NOT NULL DEFAULT '',
  `pwd` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  test01.admin 的数据：~0 rows (大约)
DELETE FROM `admin`;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;

-- 导出  表 test01.classes 结构
DROP TABLE IF EXISTS `classes`;
CREATE TABLE IF NOT EXISTS `classes` (
  `Id` decimal(10,0) NOT NULL COMMENT '班级Id',
  `name` varchar(20) NOT NULL COMMENT '名称',
  `grade` decimal(11,0) DEFAULT NULL COMMENT '年级',
  `majorId` varchar(20) DEFAULT NULL COMMENT '所属专业',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='班级表';

-- 正在导出表  test01.classes 的数据：~2 rows (大约)
DELETE FROM `classes`;
/*!40000 ALTER TABLE `classes` DISABLE KEYS */;
INSERT INTO `classes` (`Id`, `name`, `grade`, `majorId`) VALUES
	(7, '名称1', 2, '物理'),
	(8, '高二', 1, '化学');
/*!40000 ALTER TABLE `classes` ENABLE KEYS */;

-- 导出  表 test01.course 结构
DROP TABLE IF EXISTS `course`;
CREATE TABLE IF NOT EXISTS `course` (
  `id` varchar(10) NOT NULL COMMENT '课程Id',
  `name` varchar(30) DEFAULT NULL COMMENT '课程名',
  `creditour` double DEFAULT NULL COMMENT '学分',
  `semester` varchar(15) DEFAULT NULL COMMENT '开课学期',
  `classhour` decimal(11,0) DEFAULT NULL COMMENT '讲授学时',
  `practicehour` decimal(11,0) DEFAULT NULL COMMENT '实验学时',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='课程表';

-- 正在导出表  test01.course 的数据：~1 rows (大约)
DELETE FROM `course`;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` (`id`, `name`, `creditour`, `semester`, `classhour`, `practicehour`, `remark`) VALUES
	('1', '课程名2', 99.98, '1', 5, 2, '备注3');
/*!40000 ALTER TABLE `course` ENABLE KEYS */;

-- 导出  表 test01.student 结构
DROP TABLE IF EXISTS `student`;
CREATE TABLE IF NOT EXISTS `student` (
  `id` varchar(20) NOT NULL COMMENT '学号',
  `name` varchar(15) NOT NULL COMMENT '姓名',
  `pwd` varchar(20) NOT NULL COMMENT '密码',
  `classesId` decimal(11,0) NOT NULL COMMENT '班级Id',
  `stype` decimal(4,0) DEFAULT NULL COMMENT '用户类型 0一般用户1管理员',
  PRIMARY KEY (`id`),
  KEY `FK_student` (`classesId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='学生表';

-- 正在导出表  test01.student 的数据：~1 rows (大约)
DELETE FROM `student`;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` (`id`, `name`, `pwd`, `classesId`, `stype`) VALUES
	('1', 'admin', '12345', 8, 1),
	('2', 'dev', 'dev', 7, 0),
	('3', 'test', 'test', 7, 0);
/*!40000 ALTER TABLE `student` ENABLE KEYS */;

-- 导出  表 test01.sxore 结构
DROP TABLE IF EXISTS `sxore`;
CREATE TABLE IF NOT EXISTS `sxore` (
  `id` decimal(20,0) NOT NULL COMMENT '序号',
  `studentId` varchar(50) DEFAULT NULL COMMENT '学生编号',
  `courseId` varchar(10) DEFAULT NULL COMMENT '课程编号',
  `semester` varchar(15) DEFAULT NULL COMMENT '开课学期',
  `score1` double DEFAULT NULL COMMENT '一考成绩',
  `score2` double DEFAULT NULL COMMENT '二考成绩',
  `score3` double DEFAULT NULL COMMENT '三考成绩',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='成绩单';

-- 正在导出表  test01.sxore 的数据：~1 rows (大约)
DELETE FROM `sxore`;
/*!40000 ALTER TABLE `sxore` DISABLE KEYS */;
INSERT INTO `sxore` (`id`, `studentId`, `courseId`, `semester`, `score1`, `score2`, `score3`) VALUES
	(7, '2', '1', '1', 74.08, 52.36, 0),
	(8, '3', '1', '1', 100, NULL, NULL);
/*!40000 ALTER TABLE `sxore` ENABLE KEYS */;

-- 导出  表 test01.user 结构
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` varchar(8) NOT NULL COMMENT '学号',
  `name` varchar(20) NOT NULL COMMENT '姓名',
  `pwd` varchar(20) NOT NULL COMMENT '密码',
  `classesId` int(11) unsigned NOT NULL COMMENT '班级Id',
  `roleId` int(4) unsigned NOT NULL DEFAULT '0' COMMENT '0-一般用户 1-管理员',
  PRIMARY KEY (`id`),
  KEY `FK_student` (`classesId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员表';

-- 正在导出表  test01.user 的数据：~10 rows (大约)
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `name`, `pwd`, `classesId`, `roleId`) VALUES
	('16422001', '张三', '123456', 1, 0),
	('16422002', 'admin', '12345', 2, 0),
	('16422003', '王五', '123456', 1, 1),
	('16422005', '赵七', '123456', 1, 0),
	('16422006', '孙八', '123456', 2, 0),
	('16422007', '韩九', '123456', 1, 0),
	('16422008', '刘十', '123456', 2, 0),
	('16422009', '春春', '123456', 1, 1),
	('16422010', '旺财', '123456', 1, 1),
	('16422011', '迪西', '123456', 2, 0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- 导出  表 test01.user2 结构
DROP TABLE IF EXISTS `user2`;
CREATE TABLE IF NOT EXISTS `user2` (
  `Id` decimal(20,0) NOT NULL COMMENT '管理员编号',
  `Username` varchar(15) NOT NULL COMMENT '管理员账号',
  `Password` varchar(20) DEFAULT NULL COMMENT '管理员密码',
  `Name` varchar(50) DEFAULT NULL,
  `Sex` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员表';

-- 正在导出表  test01.user2 的数据：~1 rows (大约)
DELETE FROM `user2`;
/*!40000 ALTER TABLE `user2` DISABLE KEYS */;
INSERT INTO `user2` (`Id`, `Username`, `Password`, `Name`, `Sex`) VALUES
	(5, 'admin', 'admin', 'admin', NULL);
/*!40000 ALTER TABLE `user2` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
