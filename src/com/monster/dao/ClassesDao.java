/**
 * 
 */
package com.monster.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.monster.entity.Classes;
@Repository
public class ClassesDao extends SqlSessionDaoSupport {
    private static final String SQL_NAMESPACE = "com.monster.service.ClassesMapper";
    @Autowired
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){  
            super.setSqlSessionFactory(sqlSessionFactory);  
        }
    
    public Classes selectById(String id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("id", id);
        return this.getSqlSession().selectOne(SQL_NAMESPACE + ".selectByPrimaryKey", map);
    }
    public List<Map<String,Object>> selectByParms(Map<String, Object> map) {
    	return this.getSqlSession().selectList(SQL_NAMESPACE + ".selectByParams", map);
    }
    
    public List<Map<String,Object>> selectAllClasses() {
        return this.getSqlSession().selectList(SQL_NAMESPACE + ".selectAllClasses");
    }
    
    public void insertClasses(Classes classes) {
        this.getSqlSession().insert(SQL_NAMESPACE + ".insert", classes);
    }

    public void delClasses(Integer id) {
        Map<String, Integer> map = new HashMap<String, Integer>();
        map.put("id", id);
        this.getSqlSession().delete(SQL_NAMESPACE + ".deleteByPrimaryKey", map);
    }
    public void updateClasses(Classes classes) {
    	this.getSqlSession().update(SQL_NAMESPACE + ".updateByPrimaryKeySelective", classes);
    }

}
