package com.monster.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.monster.entity.Course;
import com.monster.entity.Student;
import com.monster.entity.Sxore;

/**
 * 功能描述:
 *
 * @Author loary
 * @Date 18/6/1.
 */

@Repository
public class StudentDao extends SqlSessionDaoSupport{

    private static final String SQL_NAMESPACE_STU = "com.monster.service.StudentMapper";
    private static final String SQL_NAMESPACE_COURSE = "com.monster.service.CourseMapper";
    private static final String SQL_NAMESPACE_SXORE = "com.monster.service.SxoreMapper";
    @Autowired
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){  
            super.setSqlSessionFactory(sqlSessionFactory);  
        }
    
    /**
     * student 增删改查
     * @Description: TODO
     * @author:Loary
     * @date:2018年6月3日 下午4:09:41
     * @param @param id
     * @param @return    
     * @return Student    
     * @throws
     */
    public Map selectInfoById(String id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("id", id);
        return this.getSqlSession().selectOne(SQL_NAMESPACE_STU + ".selectInfoByPrimaryKey", map);
    }
    public Student selectById(String id) {
    	Map<String, String> map = new HashMap<String, String>();
    	map.put("id", id);
    	return this.getSqlSession().selectOne(SQL_NAMESPACE_STU + ".selectByPrimaryKey", map);
    }
    public Student selectByName(String name) {
    	Map<String, String> map = new HashMap<String, String>();
    	map.put("name", name);
    	return this.getSqlSession().selectOne(SQL_NAMESPACE_STU + ".selectByName", map);
    }
    public List<Map<String,Object>> selectByParms(Map<String, Object> map) {
    	return this.getSqlSession().selectList(SQL_NAMESPACE_STU + ".selectByParams", map);
    }
    public List<Student> selectAllStudents() {
        return this.getSqlSession().selectList(SQL_NAMESPACE_STU + ".selectAllStudents");
    }

    public void updateStudent(Student student) {
    	this.getSqlSession().update(SQL_NAMESPACE_STU + ".updateByPrimaryKeySelective", student);
    }
    public void addStudent(Student student) {
        this.getSqlSession().insert(SQL_NAMESPACE_STU + ".insert", student);
    }
    public void addUploadStudent(Student student) {
    	this.getSqlSession().insert(SQL_NAMESPACE_STU + ".insertupload", student);
    }

    public void delStudent(String id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("id", id);
        this.getSqlSession().delete(SQL_NAMESPACE_STU + ".deleteByPrimaryKey", map);
    }
    
    public void delStudentSelective(List<String> list) {
    	this.getSqlSession().delete(SQL_NAMESPACE_STU + ".deleteByPrimaryKeySelective", list);
    }


    //查询已选课信息
    public List<Course> selectSelectedCoursesById(String studentId) {
        List<Sxore> sxores = selectSxoreById(studentId);
        List<String> courseIds = new ArrayList<String>();
        for(Sxore sxore:sxores){
        	courseIds.add(sxore.getCourseid());
        }
		Map<String, Object> map = new HashMap<String, Object>();
        map.put("id", courseIds);
        return this.getSqlSession().selectList(SQL_NAMESPACE_COURSE + ".selectSelectedCourses",map);
    }
    //查询可选课程
    public List<Course> selectSelectableCourses(String studentId) {
        List<Sxore> sxores = selectSxoreById(studentId);
        List<String> courseIds = new ArrayList<String>();
        for(Sxore sxore:sxores){
        	courseIds.add(sxore.getCourseid());
        }
		Map<String, Object> map = new HashMap<String, Object>();
        map.put("id", courseIds);
    	return this.getSqlSession().selectList(SQL_NAMESPACE_COURSE + ".selectSelectableCourses");
    }

	/** 
	 * @Title: selectSxoreById 
	 * @Description: TODO
	 * @author:Loary
	 * @date:2018年6月3日 下午5:20:59
	 * @param @param studentId
	 * @param @return    
	 * @return List<Sxore>    
	 * @throws 
	 */
	public List<Sxore> selectSxoreById(String studentId) {
		Map<String, Object> map = new HashMap<String, Object>();
        map.put("studentId", studentId);
        List<Sxore> sxores=  this.getSqlSession().selectList(SQL_NAMESPACE_SXORE + ".selectCoursesByStudentId");
		return sxores;
	}

    /**
     * 学生选课 
     * @Description: TODO
     * @author:Loary
     * @date:2018年6月3日 下午4:26:47
     * @param @param course    
     * @return void    
     * @throws
     */
    public void addCourse(Sxore sxore) {
	    this.getSqlSession().insert(SQL_NAMESPACE_SXORE + ".insert", sxore);
	}
    public void delCourseByCId(String id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("id", id);
        this.getSqlSession().delete(SQL_NAMESPACE_SXORE + ".delCourseByCId", map);
    }

    public void delCourseBySId(String id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("id", id);
        this.getSqlSession().delete(SQL_NAMESPACE_SXORE + ".delCourseBySId", map);
    }
    
}
