/**
 * Copyright (C) 2018 
 * @Package com.monster.dao 
 * @className:SxoreDa0
 * @description:TODO
 * @date 2018年6月3日 下午4:29:38 
 * @version:v1.0.0 
 * @author:Loary 
 * 
 * Modification History:
 * Date                Author        Version       Description
 * -----------------------------------------------------------------
 * 2018年6月3日 下午4:29:38     Loary       v1.0.0          create 
 *
 */
package com.monster.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.monster.entity.Sxore;
@Repository
public class SxoreDao extends SqlSessionDaoSupport {
	
    private static final String SQL_NAMESPACE = "com.monster.service.SxoreMapper";
    @Autowired
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){  
            super.setSqlSessionFactory(sqlSessionFactory);  
        }
    
    public Sxore selectById(String id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("id", id);
        return this.getSqlSession().selectOne(SQL_NAMESPACE + ".selectByPrimaryKey", map);
    }
    public List<Sxore> selectSxoreByStuId(String stuId) {
    	Map<String, String> map = new HashMap<String, String>();
    	map.put("id", stuId);
    	return this.getSqlSession().selectList(SQL_NAMESPACE + ".selectSxoreByStuId", map);
    }
    public List<Sxore> selectSxoreByCouId(String studentId,String courseId,String semester) {
    	Map<String, String> map = new HashMap<String, String>();
    	map.put("studentId", studentId);
    	map.put("courseId", courseId);
    	map.put("semester", semester);
    	return this.getSqlSession().selectList(SQL_NAMESPACE + ".selectSxoreByCouId", map);
    }
    public List<Map> selectByParms(Map<String, Object> map) {
    	return this.getSqlSession().selectList(SQL_NAMESPACE + ".selectByParams", map);
    }
    public List<Map> selectByParms2(Map<String, Object> map) {
    	return this.getSqlSession().selectList(SQL_NAMESPACE + ".selectByParams2", map);
    }
    public List<Sxore> selectSxoreByCouId(String studentId,String semester) {
    	Map<String, String> map = new HashMap<String, String>();
    	map.put("studentId", studentId);
    	map.put("semester", semester);
    	return this.getSqlSession().selectList(SQL_NAMESPACE + ".selectSxoreByCouId", map);
    }
    
    public List<Sxore> selectAllSxore() {
        return this.getSqlSession().selectList(SQL_NAMESPACE + ".selectAllSxore");
    }
    
    public void insertSxore(Sxore sxore) {
        this.getSqlSession().insert(SQL_NAMESPACE + ".insert", sxore);
    }

    public void delSxore(Integer id) {
        Map<String, Integer> map = new HashMap<String, Integer>();
        map.put("id", id);
        this.getSqlSession().delete(SQL_NAMESPACE + ".deleteByPrimaryKey", map);
    }
    public void updateSxoreById(Sxore sxore) {
    	this.getSqlSession().update(SQL_NAMESPACE + ".updateByPrimaryKeySelective", sxore);
    }
    public void updateSxore(Sxore sxore) {
    	this.getSqlSession().update(SQL_NAMESPACE + ".updateScoreSelective", sxore);
    }

}
