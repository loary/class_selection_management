package com.monster.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.monster.entity.Course;
@Repository
public class CourseDao extends SqlSessionDaoSupport {
    private static final String SQL_NAMESPACE_COURSE = "com.monster.service.CourseMapper";
    @Autowired
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){  
            super.setSqlSessionFactory(sqlSessionFactory);  
        }
    
    public Course selectById(String id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("id", id);
        return this.getSqlSession().selectOne(SQL_NAMESPACE_COURSE + ".selectByPrimaryKey", map);
    }
    
    public List<Course> selectAllCourses() {
        return this.getSqlSession().selectList(SQL_NAMESPACE_COURSE + ".selectAllCourses");
    }
    public List<Course> selectAllCourses(Map map) {
    	return this.getSqlSession().selectList(SQL_NAMESPACE_COURSE + ".selectAllCourses2", map);
    }
    public Double getSumScore(List<String>  ids) {
        Map map = new HashMap();
        map.put("id", ids);
    	return this.getSqlSession().selectOne(SQL_NAMESPACE_COURSE + ".selectSumScoreByIds",map);
    }
    
    public void addCourse(Course course) {
        this.getSqlSession().insert(SQL_NAMESPACE_COURSE + ".insert", course);
    }

    public void delCourse(String id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("id", id);
        this.getSqlSession().delete(SQL_NAMESPACE_COURSE + ".deleteByPrimaryKey", map);
    }
    public void updateCourse(Course course) {
    	this.getSqlSession().update(SQL_NAMESPACE_COURSE + ".updateByPrimaryKeySelective", course);
    }

}
