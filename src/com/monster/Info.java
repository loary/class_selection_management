/**
 * Copyright (C) 2018 
 * @Package com.monster 
 * @className:Info
 * @description:TODO
 * @date 2018年6月5日 下午2:15:12 
 * @version:v1.0.0 
 * @author:Loary 
 * 
 * Modification History:
 * Date                Author        Version       Description
 * -----------------------------------------------------------------
 * 2018年6月5日 下午2:15:12     Loary        v1.0.0          create 
 *
 */
package com.monster;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

public class Info extends BaseObject {
    private static final long serialVersionUID = -2486717908846610128L;
    private static final Logger logger = Logger.getLogger("com.monster.Info");
    private String name;
    /**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}
	/**
	 * @return the data
	 */
	public List getData() {
		return data;
	}
	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @param msg the msg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(List data) {
		this.data = data;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	private String msg;
    private List data;
    private int status;
	/** 
	 * <p>Title: </p> 
	 * <p>Description: </p>  
	 */
	public Info() {
		// TODO Auto-generated constructor stub
        msg = "";
        data = new ArrayList();
        status = 0;
	}
	public Map toMap(){
		Map map = new HashMap();
		map.put("name", name);
		map.put("msg", msg);
		map.put("status", status);
		map.put("data", data);
//		map.putAll(getAttr());
		return map;
	}

}
