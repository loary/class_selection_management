package com.monster.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monster.dao.CourseDao;
import com.monster.entity.Course;
@Service
public class CourseService {

	@Autowired
    private CourseDao courseDao;
	
	public List<Course> getAllCourses() {
    	return courseDao.selectAllCourses();
    }
	public List<Course> getAllCourses(Map map) {
		return courseDao.selectAllCourses(map);
	}
	public Course getCoursesByPkey(String id) {
		return courseDao.selectById(id);
	}
    public boolean addCourse(Course course) {
    	String name=course.getName();
    	if(name.length()>0&&name.length()<=100){
    		Double creditour=course.getCreditour();
    		if(creditour>=0){
    			Long classhour=course.getClasshour();
    			if(classhour>=0){
    				Long practicehour=course.getPracticehour();
    				if(practicehour>=0){
    					courseDao.addCourse(course);
    					return true;
    				}
    			}
    		}
    	}
    	return false;
    }

    
    public void delCourse(String courseId) {
    	courseDao.delCourse(courseId);
    }

    
    public Course getCourseById(String courseId) {
        return courseDao.selectById(courseId);
    }
    
    public boolean updateCourse(Course course) {
    	String id=course.getId();
        if(id!=null) {
           	String name=course.getName();
        	if(name.length()>0&&name.length()<=100){
        		Double creditour=course.getCreditour();
        		if(creditour>=0){
        			Long classhour=course.getClasshour();
        			if(classhour>=0){
        				Long practicehour=course.getPracticehour();
        				if(practicehour>=0){
        					courseDao.updateCourse(course);
        					return true;
        				}
        			}
        		}
            }
        }
        return false;
    }
}
