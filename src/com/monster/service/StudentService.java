package com.monster.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monster.ImportUserUtil;
import com.monster.dao.StudentDao;
import com.monster.entity.Course;
import com.monster.entity.Student;

/**
 * 功能描述:
 *
 * @Author loary
 * @Date 18/6/1.
 */

@Service
public class StudentService{
	
	@Autowired
	private StudentDao studentDao;
	
	private Student student;

	public Student login(String username, String pwd) {
		student = studentDao.selectByName(username);
		if (student != null && student.getPwd().equals(pwd)) {
			return student;
		}
		return null;
	}

	public List<Student> getAllStudents() {
		return studentDao.selectAllStudents();
	}

	public Student getStudentById(String id) {
		return studentDao.selectById(id);
	}
	public Map getStudentInfoById(String id) {
		return studentDao.selectInfoById(id);
	}
	
	public List<Map<String, Object>> getStudentByParams(String name,String classesid,String stype) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("name", "".equals(name)?null:name);
        classesid = "".equals(classesid)?null:classesid;
        stype = "".equals(stype)?null:stype;
        if(classesid!=null){
        	long big = new Long(classesid);
        	map.put("classesid", big);//classesid==classid
        }
        if(stype!=null){
        	short big = new Short(stype);
        	map.put("stype", big);
        }
		return studentDao.selectByParms(map);
	}

	public List<Course> getSelectableCourses(String id) {
		return studentDao.selectSelectableCourses(id);
	}

	public List<Course> getSelectedCourses(String id) {
		return studentDao.selectSelectedCoursesById(id);
	}

	public boolean updateStudent(Student student) {
		if (student.getId() != null && student.getId().length() != 0) {
			String name = student.getName().trim();
			if (name != null && name.length() <= 20 && name.length() > 0) {
				String pwd = student.getPwd().trim();
				if (pwd != null && pwd.length() <= 20 && pwd.length() > 0) {
					Long classId = student.getClassesid();
					if (classId > 0) {
						Short stype = student.getStype();
						if (stype != 1) {
							stype = 0;
							studentDao.updateStudent(student);
							return true;
						}

					}
				}
			}
		}
		return false;
	}
	
    public boolean changePwd(String stuName, String old, String newpwd) {
        Student s = login(stuName, old);
        if (s != null) {
            s.setPwd(newpwd);
            studentDao.updateStudent(s);
            return true;
        }
        return false;
    }

	public boolean addStudent(Student student) {
			String name = student.getName().trim();
			if (name != null && name.length() <= 20 && name.length() > 0) {
				String pwd = student.getPwd().trim();
				if (pwd != null && pwd.length() <= 20 && pwd.length() > 0) {
					Long classId = student.getClassesid();
					if (classId > 0) {
						Short stype = student.getStype();
						if (stype != 1) {
							stype = 0;
						}
						studentDao.addStudent(student);
						return true;
					}
				}
			}
		return false;
	}
	public boolean addStudent(String filepath) {
		List<Student> students = ImportUserUtil.exportListFromExcelFile(filepath, 0);
		boolean falg = false;
		for(Student student:students){
			String name = student.getName().trim();
			if (name != null && name.length() <= 20 && name.length() > 0) {
				String pwd = student.getPwd().trim();
				if (pwd != null && pwd.length() <= 20 && pwd.length() > 0) {
					Long classId = student.getClassesid();
					if (classId > 0) {
						Short stype = student.getStype();
						stype = stype==null?0:stype;
						if (stype != 1) {
							student.setStype((short) 0);
						}
						studentDao.addUploadStudent(student);
						falg = falg&&true;
					}
				}
			}
		}
		return falg;
	}

	public void delStudent(String id) {
		studentDao.delStudent(id);
		studentDao.delCourseBySId(id);
	}


	/**
	 * @return the student
	 */
	 public Student getStudent() {
		return student;
	}
	/**
	 * @param student the student to set
	 */
	 public void setStudent(Student student) {
		this.student = student;
	}
}
