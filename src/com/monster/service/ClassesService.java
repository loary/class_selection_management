package com.monster.service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monster.dao.ClassesDao;
import com.monster.entity.Classes;
@Service
public class ClassesService {

	@Autowired
	private ClassesDao classesDao;
	//查询
	public List<Map<String,Object>> getAllClassesList() {
		return classesDao.selectAllClasses();
	}
	//修改班级信息
	public synchronized void updateClass(Classes classes) {
		classesDao.updateClasses(classes);
	}
	
	public synchronized void delClasses(Integer id) {
		classesDao.delClasses(id);
	}
	
	public synchronized void addClasses(Classes Classes) {
		classesDao.insertClasses(Classes);
	}
	
	public List<Map<String, Object>> getClassesByParams(String name,String grade,String majorid) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("name", "".equals(name)?null:name);
        grade = "".equals(grade)?null:grade;
        if(grade!=null){
        	BigDecimal big = new BigDecimal(grade);
        	map.put("grade", big);
        }
        map.put("majorid", "".equals(majorid)?null:majorid);
		return classesDao.selectByParms(map);
	}

}
