package com.monster.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.monster.dao.CourseDao;
import com.monster.dao.SxoreDao;
import com.monster.entity.Course;
import com.monster.entity.Student;
import com.monster.entity.Sxore;
@Service
public class SxoreService {

	private static int MAX_SCORE = 15;
	@Autowired
	private SxoreDao sxoreDao;
	@Autowired
	private CourseDao courseDao;
	//查询所有学生的成绩
	public List<Sxore> getAllSxore() {
		return sxoreDao.selectAllSxore();
	}
	//选课
	public synchronized int chooseSxore(Sxore sxore) {
		List<Sxore> sxores = getSxoreByStuId(sxore.getStudentid(), sxore.getCourseid(), sxore.getSemester());
		if(sxores.size()>0){
			return 0;//课程已选
		}
		double selectCre = getSelectedSxore(sxore.getStudentid(), sxore.getSemester());
		Course course = courseDao.selectById(sxore.getCourseid());
		if(selectCre+course.getCreditour()>MAX_SCORE){
			return 2;//课程总分超过15分
		}
		sxoreDao.insertSxore(sxore);
		return 1;
	}
	
	public synchronized void delSxore(Integer id) {
		sxoreDao.delSxore(id);
	}
	public synchronized void updateSxore(Sxore sxore) {
		sxoreDao.updateSxoreById(sxore);
	}
	//成绩录入
	public void addSxore(Sxore sxore) {
		sxoreDao.updateSxore(sxore);
	}
	
	public double getSelectedSxore(String studentId,String semester) {
		List<Sxore> sxores = sxoreDao.selectSxoreByCouId(studentId, semester);
		List<String> courseIds = new ArrayList<String>();
		for(Sxore sxore:sxores){
			courseIds.add(sxore.getCourseid());
		}
		if(courseIds.size()>0){
			return courseDao.getSumScore(courseIds);
		}else{
			return 0;
		}
	}
	//根据学生ID查询
	public List<Sxore> getSxoreByStuId(String id) {
		return sxoreDao.selectSxoreByStuId(id);
	}
	//根据课程查询
	public List<Sxore> getSxoreByStuId(String studentId,String courseId,String semester) {
		return sxoreDao.selectSxoreByCouId(studentId, courseId, semester);
	}
	//根据课程查询
	public List<Map> getSxoreByParms(Map map) {
		return sxoreDao.selectByParms(map);
	}
	public List<Map> getSxoreByParms2(Map map) {
		return sxoreDao.selectByParms2(map);
	}

}
