/**
 * Copyright (C) 2018 
 * @Package com.monster 
 * @className:BaseObject1
 * @description:TODO
 * @date 2018年6月5日 下午2:05:28 
 * @version:v1.0.0 
 * @author:Loary 
 * 
 * Modification History:
 * Date                Author        Version       Description
 * -----------------------------------------------------------------
 * 2018年6月5日 下午2:05:28     Loary        v1.0.0          create 
 *
 */
package com.monster;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.log4j.Logger;

public class BaseObject implements Serializable, Cloneable {

	/** 
	 * <p>Title: </p> 
	 * <p>Description: </p>  
	 */
	public BaseObject() {
		// TODO Auto-generated constructor stub
	}
	   public String toString()
	    {
	        return (new ReflectionToStringBuilder(this)).toString();
	    }

	    public boolean equals(Object obj)
	    {
	        return EqualsBuilder.reflectionEquals(this, obj);
	    }

	    public int hashCode()
	    {
	        return HashCodeBuilder.reflectionHashCode(this);
	    }

	    public Object clone()
	    {
	        try {
				return BeanUtils.cloneBean(this);
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
	    }

	    public Object deepClone()
	    {
	        BaseObject newObj;
	        newObj = (BaseObject)clone();
	        if(attr != null)
	            newObj.setAttr((HashMap)((HashMap)attr).clone());
	        return newObj;
	    }

	    public Map getAttr()
	    {
	        return attr;
	    }

	    public void setAttr(Map attr)
	    {
	        this.attr = attr;
	    }

	    public Object get(String key)
	    {
	        return attr != null ? attr.get(key) : null;
	    }

	    public String getString(String key)
	    {
	        return get(key) != null ? String.valueOf(get(key)) : null;
	    }

	    public int getInt(String key)
	    {
	        return Integer.parseInt(String.valueOf(get(key)));
	    }

	    public String getAttrKeys()
	    {
	        if(attr == null)
	            return "";
	        StringBuffer sb = new StringBuffer();
	        for(Iterator i = attr.keySet().iterator(); i.hasNext(); sb.append((new StringBuilder()).append((String)i.next()).append(",").toString()));
	        return StringUtils.substringBeforeLast(sb.toString(), ",");
	    }

	    public void set(String key, Object value)
	    {
	        if(attr == null)
	            attr = new HashMap();
	        attr.put(key, value);
	    }

	    private static final Logger logger = Logger.getLogger("com.monster.BaseObject");
	    private static final long serialVersionUID = 4406463535024651558L;
	    private Map attr;
}
