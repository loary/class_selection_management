/**
 * Copyright (C) 2018 
 * @Package com.monster 
 * @className:ImportUserUtil
 * @description:TODO
 * @date 2018年6月7日 上午10:05:46 
 * @version:v1.0.0 
 * @author:Loary 
 * 
 * Modification History:
 * Date                Author        Version       Description
 * -----------------------------------------------------------------
 * 2018年6月7日 上午10:05:46     Loary        v1.0.0          create 
 *
 */
package com.monster;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.monster.entity.Student;
  
public class ImportUserUtil {
    /** 
     * Excel 2003 
     */  
    private final static String XLS = "xls";  
    /** 
     * Excel 2007 
     */  
    private final static String XLSX = "xlsx";  
    /** 
     * 分隔符 
     */  
    private final static String SEPARATOR = "|";  
    /**
     * 获取class路径
     * @Title: getTemplatePath 
     * @Description: TODO
     * @author:Loary
     * @date:2018年6月7日 上午11:03:45
     * @param @return    
     * @return String    
     * @throws
     */
    public static String getTemplatePath(){
		String root = Thread.currentThread().getContextClassLoader().getResource("").toString();
		String rootpath = root.substring(root.indexOf("file")+"file".length()+2);
		return rootpath.substring(0, rootpath.indexOf("WEB-INF"));
	}
    /** 
     * 由Excel文件的Sheet导出至List 
     *  
     * @param file 
     * @param sheetNum 
     * @return 
     */  
    public static List<Student> exportListFromExcelFile(String filepath, int sheetNum) {  
			return exportListFromExcel(new File(filepath) , sheetNum);
    }
    public static List<Student> exportListFromExcel(File file, int sheetNum){  
    	try {
			return exportListFromExcel(new FileInputStream(file),  
					FilenameUtils.getExtension(file.getName()), sheetNum);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ArrayList<Student>();  
    }
    
    /** 
     * 由Excel流的Sheet导出至List 
     *  
     * @param is 
     * @param extensionName 
     * @param sheetNum 
     * @return 
     * @throws IOException 
     */  
    public static List<Student> exportListFromExcel(InputStream is,  
            String extensionName, int sheetNum) throws IOException {  
  
        Workbook workbook = null;  
  
        if (extensionName.toLowerCase().equals(XLS)) {  
            workbook = new HSSFWorkbook(is);  
        } else if (extensionName.toLowerCase().equals(XLSX)) {  
            workbook = new XSSFWorkbook(is);  
        }  
  
        return exportListFromExcel(workbook, sheetNum);  
    }  
    
    /** 
     * 由指定的Sheet导出至List 
     *  
     * @param workbook 
     * @param sheetNum 
     * @return 
     * @throws IOException 
     */  
    private static List<Student> exportListFromExcel(Workbook workbook,  
            int sheetNum) {  
  
        Sheet sheet = workbook.getSheetAt(sheetNum);  
  
        // 解析公式结果  
        FormulaEvaluator evaluator = workbook.getCreationHelper()  
                .createFormulaEvaluator();  
  
        List<Student> list = new ArrayList<Student>();  
        int minRowIx = sheet.getFirstRowNum()+1;  
        int maxRowIx = sheet.getLastRowNum();  
        for (int rowIx = minRowIx; rowIx <= maxRowIx; rowIx++) {  
            Student student = new Student();  
            Row row = sheet.getRow(rowIx);  
            short minColIx = row.getFirstCellNum();  
            short maxColIx = row.getLastCellNum();  
            for (short colIx = minColIx; colIx <= maxColIx; colIx++) {  
                Cell cell = row.getCell(new Integer(colIx));  
                CellValue cellValue = evaluator.evaluate(cell);  
                if (cellValue == null) {  
                    continue;  
                }  
                switch(colIx){  
                case 0:  
                    //如果手机号码是数字格式则转换（放开注释），如果是文本则不转换  
                    DecimalFormat df = new DecimalFormat("#");  
                    String id = df.format(cellValue.getNumberValue());  
//                    String id = cellValue.getStringValue();  
                    student.setId(id);  
                    break;  
                case 1:  
                    String name=cellValue.getStringValue();  
                    student.setName(name);  
                    break;  
                case 2:  
                    DecimalFormat df11 = new DecimalFormat("#");  
                    String pwd = df11.format(cellValue.getNumberValue()); 
//                	String pwd=cellValue.getStringValue();  
                	student.setPwd(pwd);;  
                	break;  
                case 3:  
                    DecimalFormat df1 = new DecimalFormat("#");  
                    String classesId = df1.format(cellValue.getNumberValue()); 
//                	String classesId=cellValue.getStringValue();  
                	classesId="".equals(classesId)?null:classesId;
                	if(classesId!=null){
                		long classid = new Long(classesId);
                		student.setClassesid(classid);  
                	}
                	break;  
                case 4:  
                    DecimalFormat df2 = new DecimalFormat("#");  
                    String stype = df2.format(cellValue.getNumberValue());
//                	String stype=cellValue.getStringValue();  
                    stype="".equals(stype)?null:stype;
                	if(stype!=null){
                		short stype1 = new Short(stype);
                		student.setStype(stype1);  
                	}
                	break;  
                default:  
                    break;  
                  
                }  
            }  
            list.add(student);  
        }  
        return list;  
    }  

}
