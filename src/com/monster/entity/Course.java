package com.monster.entity;

import java.io.Serializable;

/**
 * course
 * @author 
 */
public class Course implements Serializable {
    /**
     * 课程Id
     */
    private String id;

    /**
     * 课程名
     */
    private String name;

    /**
     * 学分
     */
    private Double creditour;

    /**
     * 开课学期
     */
    private String semester;

    /**
     * 讲授学时
     */
    private Long classhour;

    /**
     * 实验学时
     */
    private Long practicehour;

    /**
     * 备注
     */
    private String remark;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getCreditour() {
        return creditour;
    }

    public void setCreditour(Double creditour) {
        this.creditour = creditour;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public Long getClasshour() {
        return classhour;
    }

    public void setClasshour(Long classhour) {
        this.classhour = classhour;
    }

    public Long getPracticehour() {
        return practicehour;
    }

    public void setPracticehour(Long practicehour) {
        this.practicehour = practicehour;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Course other = (Course) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getCreditour() == null ? other.getCreditour() == null : this.getCreditour().equals(other.getCreditour()))
            && (this.getSemester() == null ? other.getSemester() == null : this.getSemester().equals(other.getSemester()))
            && (this.getClasshour() == null ? other.getClasshour() == null : this.getClasshour().equals(other.getClasshour()))
            && (this.getPracticehour() == null ? other.getPracticehour() == null : this.getPracticehour().equals(other.getPracticehour()))
            && (this.getRemark() == null ? other.getRemark() == null : this.getRemark().equals(other.getRemark()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getCreditour() == null) ? 0 : getCreditour().hashCode());
        result = prime * result + ((getSemester() == null) ? 0 : getSemester().hashCode());
        result = prime * result + ((getClasshour() == null) ? 0 : getClasshour().hashCode());
        result = prime * result + ((getPracticehour() == null) ? 0 : getPracticehour().hashCode());
        result = prime * result + ((getRemark() == null) ? 0 : getRemark().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", creditour=").append(creditour);
        sb.append(", semester=").append(semester);
        sb.append(", classhour=").append(classhour);
        sb.append(", practicehour=").append(practicehour);
        sb.append(", remark=").append(remark);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}