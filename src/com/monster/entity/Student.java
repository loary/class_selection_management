package com.monster.entity;

import java.io.Serializable;

/**
 * student
 * @author 
 */
public class Student implements Serializable {
    /**
     * 学号
     */
    private String id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 密码
     */
    private String pwd;

    /**
     * 班级Id
     */
    private Long classesid;

    /**
     * 用户类型 0一般用户1管理员
     */
    private Short stype;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public Long getClassesid() {
        return classesid;
    }

    public void setClassesid(Long classesid) {
        this.classesid = classesid;
    }

    public Short getStype() {
        return stype;
    }

    public void setStype(Short stype) {
        this.stype = stype;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Student other = (Student) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getPwd() == null ? other.getPwd() == null : this.getPwd().equals(other.getPwd()))
            && (this.getClassesid() == null ? other.getClassesid() == null : this.getClassesid().equals(other.getClassesid()))
            && (this.getStype() == null ? other.getStype() == null : this.getStype().equals(other.getStype()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getPwd() == null) ? 0 : getPwd().hashCode());
        result = prime * result + ((getClassesid() == null) ? 0 : getClassesid().hashCode());
        result = prime * result + ((getStype() == null) ? 0 : getStype().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", pwd=").append(pwd);
        sb.append(", classesid=").append(classesid);
        sb.append(", stype=").append(stype);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}