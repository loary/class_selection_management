package com.monster.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.monster.AuthUtil;
import com.monster.Info;
import com.monster.entity.Classes;
import com.monster.service.ClassesService;

/**
 * 功能描述:
 *
 * @Author Loary
 * @Date 18/6/2.
 */

@Controller
public class ClassesController {

	@Autowired
	private ClassesService classesService;
	@RequestMapping("/managerClass")
	public String managerClass(HttpServletRequest req) {
		String stuId = (String) req.getSession().getAttribute("userId");
    	if(stuId!=null){
    		String msg = (String) req.getSession().getAttribute("msg");
    		req.getSession().setAttribute("msg",msg!=null?msg:"欢迎您!");
    		return "classManager";
    	}else{
    		req.getSession().setAttribute("msg", null);
    		return "login";
    	}
	}

	@RequestMapping("/queryClass")
	@ResponseBody
	public Info queryClass(HttpServletRequest req) {
		Info info = new Info();
		if(AuthUtil.checkAuth(req)){
			String sId = (String) req.getSession().getAttribute("userId");
			Short stype =  (Short) req.getSession().getAttribute("stype");
			String name = (String) req.getParameter("name");
			String grade = (String) req.getParameter("grade");
			String majorid = (String) req.getParameter("majorid");
			List<Map<String, Object>> classes= classesService.getClassesByParams(name, grade, majorid);
			info.setStatus(AuthUtil.SUCCESS);
			info.setMsg("查询成功！");
			info.setData(classes);
		}else{
			info.setMsg("查询失败，无权操作！");
			info.setStatus(AuthUtil.ERROR);
		}
		return info;
	}
	@RequestMapping("/selectClass")
	@ResponseBody
	public Info selectClass(HttpServletRequest req) {
		Info info = new Info();
			List<Map<String, Object>> classes= classesService.getAllClassesList();
			info.setStatus(AuthUtil.SUCCESS);
			info.setMsg("查询成功！");
			info.setData(classes);
		return info;
	}

	@RequestMapping("/delClass")
	@ResponseBody
	public synchronized Info delClass(HttpServletRequest req){
		Info info = new Info();
		if(AuthUtil.checkAuth(req)){
			String classId =(String) req.getParameter("id");
			if(classId!=null){
				int id = Integer.parseInt(classId);
				classesService.delClasses(id);
				info.setStatus(AuthUtil.SUCCESS);
				info.setMsg("删除成功！");
			}
		}else{
			info.setMsg("删除失败，无权操作！");
			info.setStatus(AuthUtil.FAILURE);
		}
		return info;
	}
	@RequestMapping("/addClass")
	@ResponseBody
	public synchronized Info addClass(HttpServletRequest req){
		Info info = new Info();
		if(AuthUtil.checkAuth(req)){
			String name = (String) req.getParameter("name");
			String grade = (String) req.getParameter("grade");
			String majorid = (String) req.getParameter("majorid");
			Classes classes = new Classes();
			if(grade!=null){
				long gradeLong= new Long(grade);
				classes.setGrade(gradeLong);
			}
			classes.setMajorid(majorid);
			classes.setName(name);
			classesService.addClasses(classes);
			info.setStatus(AuthUtil.SUCCESS);
			info.setMsg("新增成功！");
		}else{
			info.setMsg("新增失败，无权操作！");
			info.setStatus(AuthUtil.FAILURE);
		}
		return info;
	}
	@RequestMapping("/editClass")
	@ResponseBody
	public synchronized Info editClass(HttpServletRequest req){
		Info info = new Info();
		if(AuthUtil.checkAuth(req)){
			String id = (String) req.getParameter("id");
			String name = (String) req.getParameter("name");
			String grade = (String) req.getParameter("grade");
			String majorid = (String) req.getParameter("majorid");
			Classes classes = new Classes();
			if(grade!=null){
				long gradeLong= new Long(grade);
				classes.setGrade(gradeLong);
			}
			if(id!=null){
				long idLong= new Long(id);
				classes.setId(idLong);
			}
			classes.setMajorid(majorid);
			classes.setName(name);
			classesService.updateClass(classes);
			info.setStatus(AuthUtil.SUCCESS);
			info.setMsg("修改成功！");
		}else{
			info.setMsg("修改失败，无权操作！");
			info.setStatus(AuthUtil.FAILURE);
		}
		return info;
	}


}
