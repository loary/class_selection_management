package com.monster.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.monster.entity.Student;
import com.monster.service.StudentService;

/**
 * Created by Loary on 18/6/2.
 */

@Controller
public class LoginController {

    @Autowired
    private StudentService studentService;

    @RequestMapping(value={"/loginin"})
    public String userLogin(@RequestParam String id, @RequestParam String pwd, HttpServletRequest req) {
        Student student = null;
        if (id.length() > 0 && id.length() < 20 && pwd.length() > 0 && pwd.length() < 20) {
            student = studentService.login(id, pwd);
        }
        if (student != null) {
            req.getSession().setAttribute("user", student.getName());
            req.getSession().setAttribute("userId", student.getId());
            req.getSession().setAttribute("stype", student.getStype());//0 一般用户 1 管理员
            req.getSession().setAttribute("msg","登录成功!欢迎您 "+student.getName()+"!");
            req.getSession().setAttribute("status", "1");
        } else {
            req.getSession().setAttribute("msg", "登录失败!用户名或密码错误!");
            req.getSession().setAttribute("status", "-1");
            return "login";
        }
        return "redirect:/index";
    }

    @RequestMapping({"/login","/index","/"})
    public String weolcome(HttpServletRequest req) {
    	String stuId = (String) req.getSession().getAttribute("userId");
    	if(stuId!=null){
    		String msg = (String) req.getSession().getAttribute("msg");
    		req.getSession().setAttribute("msg",msg!=null?msg:"欢迎您!");
    		return "index";
    	}else{
    		req.getSession().setAttribute("msg", null);
    		return "login";
    	}
    }
    @RequestMapping("/logout")
    public String userLogout(HttpServletRequest req) {
        req.getSession().setAttribute("user", null);
        req.getSession().setAttribute("userId", null);
        return "login";
    }

    @RequestMapping("/changePwd")
    public String changePwd(HttpServletRequest req, @RequestParam String old,
                            @RequestParam String newpwd, @RequestParam String newagain) {
        String stuName = (String) req.getSession().getAttribute("user");
        if (newpwd.equals(newagain) && studentService.changePwd(stuName, old, newpwd)) {
            req.getSession().setAttribute("msg", "密码修改成功!");
            req.getSession().setAttribute("status", "1");
        } else {
            req.getSession().setAttribute("msg", "密码修改失败!");
            req.getSession().setAttribute("status", "-1");
        }
        return "redirect:/index";
    }

}
