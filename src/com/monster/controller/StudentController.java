package com.monster.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.monster.AuthUtil;
import com.monster.ImportUserUtil;
import com.monster.Info;
import com.monster.entity.Student;
import com.monster.service.StudentService;

/**
 * 功能描述:
 *
 * @Author Loary
 * @Date 18/6/2.
 */

@Controller
public class StudentController {

	@Autowired
	private StudentService studentService;
	@RequestMapping("/managerStudent")
	public String managerClass(HttpServletRequest req) {
		return "studentManager";
//		String stuId = (String) req.getSession().getAttribute("userId");
//    	if(stuId!=null){
//    		String msg = (String) req.getSession().getAttribute("msg");
//    		req.getSession().setAttribute("msg",msg!=null?msg:"欢迎您!");
//    		return "studentManager";
//    	}else{
//    		req.getSession().setAttribute("msg", null);
//    		return "login";
//    	}
	}

	@RequestMapping("/queryStudent")
	@ResponseBody
	public Info queryStudent(HttpServletRequest req) {
		Info info = new Info();
		if(AuthUtil.checkAuth(req)){
			String sId = (String) req.getSession().getAttribute("userId");
			String name = (String) req.getParameter("name");
			String classesid = (String) req.getParameter("classesid");
			String stype = (String) req.getParameter("stype");
			List<Map<String, Object>> students= studentService.getStudentByParams(name,classesid,stype);
			info.setStatus(AuthUtil.SUCCESS);
			info.setMsg("查询成功！");
			info.setData(students);
		}else{
			info.setMsg("查询失败，无权操作！");
			info.setStatus(AuthUtil.ERROR);
		}
		return info;
	}
	@RequestMapping("/fileDownLoad")
	public ResponseEntity<byte[]> fileDownLoad(HttpServletRequest request) throws Exception{
		String basepath = ImportUserUtil.getTemplatePath();
		String fileName="import_model.xls";
		String realPath = basepath+"/WEB-INF/"+fileName;//得到文件所在位置
		InputStream in=new FileInputStream(new File(realPath));//将该文件加入到输入流之中
		byte[] body=null;
		body=new byte[in.available()];// 返回下一次对此输入流调用的方法可以不受阻塞地从此输入流读取（或跳过）的估计剩余字节数
		in.read(body);//读入到输入流里面

		fileName=new String(fileName.getBytes("gbk"),"iso8859-1");//防止中文乱码
		HttpHeaders headers=new HttpHeaders();//设置响应头
		headers.add("Content-Disposition", "attachment;filename="+fileName);
		HttpStatus statusCode = HttpStatus.OK;//设置响应吗
		ResponseEntity<byte[]> response=new ResponseEntity<byte[]>(body, headers, statusCode);
		return response;
	}
    @RequestMapping(value = "/uploadFile")
    @ResponseBody
    public  Info uploadFileHandler( HttpServletRequest req,@RequestParam("file") MultipartFile file) {
		Info info = new Info();
		if(AuthUtil.checkAuth(req)){
			if (!file.isEmpty()) {
	            try {
	                // 文件存放服务端的位置
	        		String basepath = ImportUserUtil.getTemplatePath();
	                String rootPath = basepath+"/filetmp";
	                File dir = new File(rootPath + File.separator + "tmpFiles");
	                if (!dir.exists())
	                    dir.mkdirs();
	                // 写文件到服务器
	                String filename = dir.getAbsolutePath() + File.separator + file.getOriginalFilename();
	                File serverFile = new File(filename);
	                file.transferTo(serverFile);
	                if(studentService.addStudent(filename)){
	                	info.setStatus(AuthUtil.SUCCESS);
	        			info.setMsg("导入成功！");
	                }else{
	        			info.setMsg("导入失败，数据不符合要求！");
	        			info.setStatus(AuthUtil.ERROR);
	                };
	            } catch (Exception e) {
	    			info.setMsg("导入失败," + e.getMessage());
	    			info.setStatus(AuthUtil.ERROR);
	            }
	        } else {
				info.setMsg("上传文件为空!");
				info.setStatus(AuthUtil.ERROR);
	        }
		}else{
			info.setMsg("导入失败，无权操作！");
			info.setStatus(AuthUtil.ERROR);
		}
		
		return info;
    }
	
	@RequestMapping("/queryStudentInfo")
	@ResponseBody
	public Info queryStudentInfo(HttpServletRequest req) {
		Info info = new Info();
			String sId = (String) req.getSession().getAttribute("userId");
			Map students= studentService.getStudentInfoById(sId);
			List list = new ArrayList();
			list.add(students);
			info.setStatus(AuthUtil.SUCCESS);
			info.setMsg("查询成功！");
			info.setData(list);
		return info;
	}

	@RequestMapping("/delStudent")
	@ResponseBody
	public synchronized Info delStudent(HttpServletRequest req){
		Info info = new Info();
		if(AuthUtil.checkAuth(req)){
			String sId = (String) req.getSession().getAttribute("userId");
			String stuId =(String) req.getParameter("id");
			if(stuId!=null){
				if(stuId.equals(sId)){
					info.setMsg("删除失败，不能删除自己！");
					info.setStatus(AuthUtil.FAILURE);
				}else{
					studentService.delStudent(stuId);
					info.setStatus(AuthUtil.SUCCESS);
					info.setMsg("删除成功！");
				}
			}
		}else{
			info.setMsg("删除失败，无权操作！");
			info.setStatus(AuthUtil.FAILURE);
		}
		return info;
	}
	@RequestMapping("/addStudent")
	@ResponseBody
	public synchronized Info addStudent(HttpServletRequest req){
		Info info = new Info();
		if(AuthUtil.checkAuth(req)){
			String id = (String) req.getParameter("id");
			String classId = (String) req.getParameter("classesid");
			String name = (String) req.getParameter("name");
			String pwd = (String) req.getParameter("pwd");
			String stype = (String) req.getParameter("stype");
			Student student = new Student();
			if(stype!=null){
				short stypeShort= new Short(stype);
				student.setStype(stypeShort);
			}
			if(classId!=null){
				long classIdLong= new Long(classId);
				student.setClassesid(classIdLong);
			}
			student.setId(id);
			student.setName(name);
			student.setPwd(pwd);
			if(studentService.addStudent(student)){
				info.setStatus(AuthUtil.SUCCESS);
				info.setMsg("新增成功！");
			}else{
				info.setStatus(AuthUtil.FAILURE);
				info.setMsg("新增失败！");
			}
		}else{
			info.setMsg("新增失败，无权操作！");
			info.setStatus(AuthUtil.FAILURE);
		}
		return info;
	}
	@RequestMapping("/editStudent")
	@ResponseBody
	public synchronized Info editStudent(HttpServletRequest req){
		Info info = new Info();
		if(AuthUtil.checkAuth(req)){
			String id = (String) req.getParameter("id");
			String classId = (String) req.getParameter("classesid");
			String name = (String) req.getParameter("name");
			String pwd = (String) req.getParameter("pwd");
			String stype = (String) req.getParameter("stype");
			Student student = new Student();
			if(stype!=null){
				short stypeShort= new Short(stype);
				student.setStype(stypeShort);
			}
			if(classId!=null){
				long classIdLong= new Long(classId);
				student.setClassesid(classIdLong);
			}
			student.setId(id);
			student.setName(name);
			student.setPwd(pwd);
			studentService.updateStudent(student);
			info.setStatus(AuthUtil.SUCCESS);
			info.setMsg("修改成功！");
		}else{
			info.setMsg("修改失败，无权操作！");
			info.setStatus(AuthUtil.FAILURE);
		}
		return info;
	}


}
