package com.monster.controller;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.monster.AuthUtil;
import com.monster.Info;
import com.monster.entity.Classes;
import com.monster.entity.Course;
import com.monster.entity.Sxore;
import com.monster.service.CourseService;
import com.monster.service.SxoreService;

/**
 * 功能描述:
 *
 * @Author Loary
 * @Date 18/6/2.
 */

@Controller
public class CourseController {

	@Autowired
	private SxoreService sxoreService;
	@Autowired
	private CourseService courseService;

	@RequestMapping("/courseManager")
	public String managerClass(HttpServletRequest req) {
		String stuId = (String) req.getSession().getAttribute("userId");
		if(stuId!=null){
			String msg = (String) req.getSession().getAttribute("msg");
			req.getSession().setAttribute("msg",msg!=null?msg:"欢迎您!");
			return "courseManager";
		}else{
			req.getSession().setAttribute("msg", null);
			return "login";
		}
	}
	@RequestMapping("/courseChoose")
	public String courseChoose(HttpServletRequest req) {
		String stuId = (String) req.getSession().getAttribute("userId");
		if(stuId!=null){
			String msg = (String) req.getSession().getAttribute("msg");
			req.getSession().setAttribute("msg",msg!=null?msg:"欢迎您!");
			return "courseChoose";
		}else{
			req.getSession().setAttribute("msg", null);
			return "login";
		}
	}

	@RequestMapping("/queryCourse")
	@ResponseBody
	public Info queryCourse(HttpServletRequest req) {
		Info info = new Info();
		List<Course> course= courseService.getAllCourses();
		info.setStatus(AuthUtil.SUCCESS);
		info.setMsg("查询成功！");
		info.setData(course);
		return info;
	}
	@RequestMapping("/querySelectCourse")
	@ResponseBody
	public Info queryCourseByParams(HttpServletRequest req) {
		Info info = new Info();
		String name = (String) req.getParameter("name");
		String semester = (String) req.getParameter("semester");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("semester", "".equals(semester)?null:semester);
		map.put("name", "".equals(name)?null:name);
		List<Course> course= courseService.getAllCourses(map);
		info.setStatus(AuthUtil.SUCCESS);
		info.setMsg("查询成功！");
		info.setData(course);
		return info;
	}
	//选课修改
	@RequestMapping("/editChoosedCourse")
	@ResponseBody
	public synchronized Info editChoosedCourse(HttpServletRequest req){
		Info info = new Info();
		String sId = (String) req.getSession().getAttribute("userId");
		String studentid = (String) req.getParameter("studentid");
		if(AuthUtil.checkAuth(req)||sId.equals(studentid)){
			String id = (String) req.getParameter("id");
			String courseid = (String) req.getParameter("courseid");
			String semester = (String) req.getParameter("semester");
			Sxore  sxore = new Sxore();
			sxore.setSemester(semester);
			sxore.setCourseid(courseid);
			sxore.setStudentid(studentid);
			if(id!=null){
				BigDecimal gradeLong= new BigDecimal(id);
				sxore.setId(gradeLong);
			}
			String msg = null;
			boolean flag = false;
			List courses = sxoreService.getSxoreByStuId(studentid, courseid, semester);
			if(courses.size()>0){
				msg = "已经选过此课!";
			}else{
				sxoreService.updateSxore(sxore);
				msg="选课成功!";
				flag = true;
			}
			info.setStatus(flag?AuthUtil.SUCCESS:AuthUtil.FAILURE);
			info.setMsg(msg);
		}else{
			info.setMsg("选课修改失败，无权操作！");
			info.setStatus(AuthUtil.FAILURE);
		}
		return info;
	}
	//退课
	@RequestMapping("/delChoosedCourse")
	@ResponseBody
	public Info delChoosedCourse(HttpServletRequest req) {
		String sId = (String) req.getSession().getAttribute("userId");
		String id = (String) req.getParameter("id");
		Info info = new Info();
		if(AuthUtil.checkAuth(req)){
			if(id!=null){
				sxoreService.delSxore(Integer.parseInt(id));
				info.setStatus(AuthUtil.SUCCESS);
				info.setMsg("退课成功！");
			}else{
				info.setMsg("退课失败，选课记录不存在！");
				info.setStatus(AuthUtil.FAILURE);
			}
		}else{
			String studentid = (String) req.getParameter("studentid");
			if(!sId.equals(studentid)){
				info.setStatus(AuthUtil.FAILURE);
				info.setMsg("退课失败，仅能操作自己的信息！");
			}else{
				if(id!=null){
					sxoreService.delSxore(Integer.parseInt(id));
					info.setStatus(AuthUtil.SUCCESS);
					info.setMsg("退课成功！");
				}else{
					info.setMsg("退课失败，选课记录不存在！");
					info.setStatus(AuthUtil.FAILURE);
				}
			}
		}
		return info;
	}
	@RequestMapping("/chooseCourse")
	@ResponseBody
	public Info chooseCourse(HttpServletRequest req) {
		String userId = (String) req.getSession().getAttribute("userId");
		Info info = new Info();
		if(AuthUtil.checkAuth(req)){
			String cId = (String) req.getParameter("courseid");
			Course course = courseService.getCoursesByPkey(cId);
			Sxore sxore = new Sxore();
			sxore.setCourseid(cId);
			sxore.setStudentid(userId);
			sxore.setSemester(course.getSemester());
			String msg = null;
			int rst = sxoreService.chooseSxore(sxore);
			boolean flag = false;
			if (rst==0) {
				msg = "已经选过此课!";
			} else if(rst==2){
				msg = "选课失败，本学期所选课程总分不能超过15分！";
			} else if(rst==1){
				msg="选课成功!";
				flag = true;
			}else{
				msg="未知错误!";
			}
			info.setStatus(flag?AuthUtil.SUCCESS:AuthUtil.FAILURE);
			info.setMsg(msg);
		}else{
			String cId = (String) req.getParameter("courseid");
			Course course = courseService.getCoursesByPkey(cId);
			Sxore sxore = new Sxore();
			sxore.setCourseid(cId);
			sxore.setStudentid(userId);
			sxore.setSemester(course.getSemester());
			String msg = null;
			int rst = sxoreService.chooseSxore(sxore);
			boolean flag = false;
			if (rst==0) {
				msg = "已经选过此课!";
			} else if(rst==2){
				msg = "选课失败，本学期所选课程总分不能超过15分！";
			} else if(rst==1){
				msg="选课成功!";
				flag = true;
			}else{
				msg="未知错误!请联系管理员";
			}
			info.setStatus(flag?AuthUtil.SUCCESS:AuthUtil.FAILURE);
			info.setMsg(msg);
		}
		return info;
	}

	@RequestMapping("/addCourse")
	@ResponseBody
	public synchronized Info addCourse(HttpServletRequest req){
		Info info = new Info();
		if(AuthUtil.checkAuth(req)){
			String name = (String) req.getParameter("name");
			String creditour = (String) req.getParameter("creditour");
			String semester = (String) req.getParameter("semester");
			String classhour = (String) req.getParameter("classhour");
			String practicehour = (String) req.getParameter("practicehour");
			String remark = (String) req.getParameter("remark");
			Course course = new Course();
			course.setRemark(remark);
			course.setName(name);
			course.setSemester(semester);
			if(creditour!=null){
				double creditour1= new Double(creditour);
				course.setCreditour(creditour1);
			}
			if(practicehour!=null){
				long practicehour1= new Long(practicehour);
				course.setPracticehour(practicehour1);
			}
			if(classhour!=null){
				long classhour1= new Long(classhour);
				course.setClasshour(classhour1);
			}
			if(courseService.addCourse(course)){
				info.setStatus(AuthUtil.SUCCESS);
				info.setMsg("新增成功！");
			}else{
				info.setMsg("新增失败！");
				info.setStatus(AuthUtil.FAILURE);
			}
		}else{
			info.setMsg("新增失败，无权操作！");
			info.setStatus(AuthUtil.FAILURE);
		}
		return info;
	}
	@RequestMapping("/editCourse")
	@ResponseBody
	public synchronized Info editClass(HttpServletRequest req){
		Info info = new Info();
		if(AuthUtil.checkAuth(req)){
			String id = (String) req.getParameter("id");
			String name = (String) req.getParameter("name");
			String creditour = (String) req.getParameter("creditour");
			String semester = (String) req.getParameter("semester");
			String classhour = (String) req.getParameter("classhour");
			String practicehour = (String) req.getParameter("practicehour");
			String remark = (String) req.getParameter("remark");
			Course course = new Course();
			course.setRemark(remark);
			course.setName(name);
			course.setSemester(semester);
			course.setId(id);
			if(creditour!=null){
				double creditour1= new Double(creditour);
				course.setCreditour(creditour1);
			}
			if(practicehour!=null){
				long practicehour1= new Long(practicehour);
				course.setPracticehour(practicehour1);
			}
			if(classhour!=null){
				long classhour1= new Long(classhour);
				course.setClasshour(classhour1);
			}
			if(courseService.updateCourse(course)){
				info.setStatus(AuthUtil.SUCCESS);
				info.setMsg("修改成功！");
			}else{
				info.setMsg("修改失败！");
				info.setStatus(AuthUtil.FAILURE);
			}
		}else{
			info.setMsg("修改失败，无权操作！");
			info.setStatus(AuthUtil.FAILURE);
		}
		return info;
	}
	@RequestMapping("/delCourse")
	public synchronized Info delCourse(HttpServletRequest req){
		Info info = new Info();
		try {
			//            String stuId = (String) req.getSession().getAttribute("userId");
			String sxoreId = (String) req.getParameter("id");
			courseService.delCourse(sxoreId);
			info.setStatus(AuthUtil.SUCCESS);
			info.setMsg("删除成功！");
		}catch (NumberFormatException e){
			info.setMsg("删除失败！");
			info.setStatus(AuthUtil.FAILURE);
		}
		return info;
	}

}
