package com.monster.controller;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.monster.AuthUtil;
import com.monster.Info;
import com.monster.entity.Sxore;
import com.monster.service.SxoreService;

/**
 * 功能描述:
 *
 * @Author Loary
 * @Date 18/6/2.
 */

@Controller
public class SxoreController {

	@Autowired
	private SxoreService sxoreService;
	
	@RequestMapping("/printSxore")
	public String managerSxore(HttpServletRequest req) {
		return "jidisnManager";
	}
	@RequestMapping("/managerSxore")
	public String managerClass(HttpServletRequest req) {
		return "sxoreManager";
	}

	@RequestMapping("/querySxore")
	@ResponseBody
	public Info querySxore(HttpServletRequest req) {
		Info info = new Info();
		if(AuthUtil.checkAuth(req)){
			String uId = (String) req.getSession().getAttribute("userId");
			String classesid = (String) req.getParameter("classesid");
			String studentid = (String) req.getParameter("studentid");
			String semester = (String) req.getParameter("semester");
			 Map<String, Object> map = new HashMap<String, Object>();
		        map.put("classesid", "".equals(classesid)?null:classesid);
		        map.put("studentid", "".equals(studentid)?null:studentid);
		        map.put("semester", "".equals(semester)?null:semester);
			List<Map> sxore= sxoreService.getSxoreByParms(map);
			info.setStatus(AuthUtil.SUCCESS);
			info.setMsg("查询成功！");
			info.setData(sxore);
		}else{
			String sId = (String) req.getSession().getAttribute("userId");
			String studentid = (String) req.getParameter("studentid");
			studentid = "".equals(studentid)?null:studentid;
			if(studentid!=null&&!sId.equals(studentid)){
				info.setStatus(AuthUtil.FAILURE);
				info.setMsg("仅能查询自己的信息！");
			}else{
				String semester = (String) req.getParameter("semester");
				String classesid = (String) req.getParameter("classesid");
				Map<String, Object> map = new HashMap<String, Object>();
		        map.put("classesid", "".equals(classesid)?null:classesid);
		        map.put("studentid", sId);
		        map.put("semester", "".equals(semester)?null:semester);
				List<Map> sxore= sxoreService.getSxoreByParms(map);
				info.setStatus(AuthUtil.SUCCESS);
				info.setMsg("查询成功！");
				info.setData(sxore);
			}
		}
		return info;
	}
	@RequestMapping("/querySxore2")
	@ResponseBody
	public Info querySxore2(HttpServletRequest req) {
		Info info = new Info();
		if(AuthUtil.checkAuth(req)){
			String uId = (String) req.getSession().getAttribute("userId");
			String classesid = (String) req.getParameter("classesid");
			String studentid = (String) req.getParameter("studentid");
			String semester = (String) req.getParameter("semester");
			semester = "".equals(semester)?null:semester;
			studentid = "".equals(studentid)?null:studentid;
			classesid = "".equals(classesid)?null:classesid;
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("classesid", classesid);
			map.put("studentid", studentid);
			map.put("semester", semester);
			List<Map> sxore= sxoreService.getSxoreByParms2(map);
			info.setStatus(AuthUtil.SUCCESS);
			info.setMsg("查询成功！");
			info.setData(sxore);
		}else{
			String sId = (String) req.getSession().getAttribute("userId");
			String semester = (String) req.getParameter("semester");
			String classesid = (String) req.getParameter("classesid");
			semester = "".equals(semester)?null:semester;
			classesid = "".equals(semester)?null:classesid;
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("classesid", classesid);
			map.put("studentid", sId);
			map.put("semester", semester);
			List<Map> sxore= sxoreService.getSxoreByParms2(map);
			info.setStatus(AuthUtil.SUCCESS);
			info.setMsg("查询成功！");
			info.setData(sxore);
		}
		return info;
	}

	@RequestMapping("/delSxore")
	@ResponseBody
	public synchronized Info delSxore(HttpServletRequest req){
		Info info = new Info();
		if(AuthUtil.checkAuth(req)){
			String sId = (String) req.getSession().getAttribute("userId");
			String xid =(String) req.getParameter("id");
			if(xid!=null){
					int xIdInt = new Integer(xid);
					sxoreService.delSxore(xIdInt);
					info.setStatus(AuthUtil.SUCCESS);
					info.setMsg("删除成功！");
			}
		}else{
			info.setMsg("删除失败，无权操作！");
			info.setStatus(AuthUtil.FAILURE);
		}
		return info;
	}
	@RequestMapping("/editSxore")
	@ResponseBody
	public synchronized Info editSxore(HttpServletRequest req){
		Info info = new Info();
		if(AuthUtil.checkAuth(req)){
			String id = (String) req.getParameter("id");
			String courseid = (String) req.getParameter("courseid");
			String studentid = (String) req.getParameter("studentid");
			String semester = (String) req.getParameter("semester");
			String score1 = (String) req.getParameter("score1");
			String score2 = (String) req.getParameter("score2");
			String score3 = (String) req.getParameter("score3");
			score1 = "".equals(score1)?null:score1;
			score2 = "".equals(score2)?null:score2;
			score3 = "".equals(score3)?null:score3;
			Sxore  sxore = new Sxore();
			sxore.setSemester(semester);
			sxore.setCourseid(courseid);
			sxore.setStudentid(studentid);
			if(score1!=null){
				double scoreLong= new Double(score1);
				sxore.setScore1(scoreLong);
			}
			if(score2!=null){
				double scoreLong= new Double(score2);
				sxore.setScore2(scoreLong);
			}
			if(score3!=null){
				double scoreLong= new Double(score3);
				sxore.setScore3(scoreLong);
			}
			if(id!=null){
				BigDecimal gradeLong= new BigDecimal(id);
				sxore.setId(gradeLong);
			}
			sxoreService.addSxore(sxore);
			info.setStatus(AuthUtil.SUCCESS);
			info.setMsg("成绩录入成功！");
		}else{
			info.setMsg("成绩录入失败，无权操作！");
			info.setStatus(AuthUtil.FAILURE);
		}
		return info;
	}


}
