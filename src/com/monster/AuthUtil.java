/**
 * Copyright (C) 2018 
 * @Package com.monster 
 * @className:AuthUtil
 * @description:TODO
 * @date 2018年6月5日 下午2:00:23 
 * @version:v1.0.0 
 * @author:Loary 
 * 
 * Modification History:
 * Date                Author        Version       Description
 * -----------------------------------------------------------------
 * 2018年6月5日 下午2:00:23     Loary        v1.0.0          create 
 *
 */
package com.monster;

import javax.servlet.http.HttpServletRequest;

public class AuthUtil {
	public static int SUCCESS = 1;
	public static int FAILURE = 0;
	public static int ERROR = -1;

	/** 
	 * @Title: checkAuth 
	 * @Description: TODO
	 * @author:Loary
	 * @date:2018年6月4日 上午12:26:56
	 * @param @param req    
	 * @return void    
	 * @throws 
	 */
	public static boolean checkAuth(HttpServletRequest req) {
		Short stype = (Short) req.getSession().getAttribute("stype");
		if(stype!=null){
			if(1==stype){
				return true;
			}
			return false;
		}else{
			return false;
		}
	}

}
